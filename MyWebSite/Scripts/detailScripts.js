﻿var editor; var ebutton; //var etitle; var _title;
var _content;
var ck_toolbar = [['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', ], ['Custom_Smiley', 'Image'],
                        ['TextColor', 'BGColor', 'Styles']];
function editreply(id) {
    if (editor) {
        editor.setData(_content);
        _content = '';
        editor.destroy();
    }
    if (ebutton){
        ebutton.hide();
        ebutton = null;
    }
    editor = CKEDITOR.replace($("#postcontent" + id + ">.reply")[0], {
        toolbar: ck_toolbar ,
        removePlugins: 'elementspath',
        resize_enabled: false
    });
    ebutton = $("#btnarea_"+id);
    ebutton.show();
    //etitle = document.getElementById("blogtitle");

    _content = editor.getData();
}
function reply(id,data) {
    var area = $('#postcontent' + id).find('textarea')[0];
    if (editor) {
        editor.setData(_content);
        _content = '';
        editor.destroy();
    }
    if (ebutton) {
        ebutton.hide();
        ebutton = null;
    }
    editor = CKEDITOR.replace(area, {
        toolbar: ck_toolbar,
        removePlugins: 'elementspath',
        resize_enabled: false
    });
    ebutton = $('#postcontent' + id).find('.addReply');
    ebutton.show();
    if (data)
        editor.setData(data);
}

function replyreply(rid, author) {
    var pid = $('#reply' + rid).parent().attr('data-postid');
    reply(pid, '回复 '+author+'：');
}

function replyPost(id) {
    if (!editor.checkDirty())
        return false;
    $.post(replyposturl, { postid: id, addreplycontent: editor.getData() }, partial(replySuccess, id));
    return false;
}

function editclick(id, actionUrl) {
    if (!editor.checkDirty())
        return false;

    $.post(actionUrl, { PostId: id, content: editor.getData() }, partial(editSuccess,id));
    return false;
}

function cancelclick() {
    editor.setData(_content);
    _content = '';
    editor.destroy();
    editor = null;
    //etitle.innerHtml = _title;
    ebutton.hide();
    return false;
}
function editSuccess(id,response) {
    if (response.errmsg) {
        $("#editerror_" + id).html(response.errmsg);
    }
    else {
        editor.destroy();
        editor = null;
        ebutton.hide();
    }
}
function replySuccess(id, response) {
    if (response.errmsg) {
        alert(response.errmsg);
    }
    else if(response.id){
        window.location.hash = ('#reply' + response.id)
        window.location.reload();
    }
}

function hidepost(id) {
    var e = document.getElementById("listpost"+id);
    e.style.display = 'none';
}

function hidereply(id) {
    var e = document.getElementById("reply" + id);
    var p = e.parentNode
    p.removeChild(e);
    if(!$.trim($(p).html())){
        $(p).addClass("empty");
    }
}

function updaterate(str) {
    $("#currentrating").text(str);
}
function ratefail(error) {
    var e = document.getElementById("ratingmsg");
    e.innerText = error.get_message();
    e.style.display = null;
    e.className = "ratefail";
    rated = true;
}
function ratesuccess(result) {
    var e = document.getElementById("ratingmsg");
    if (result.errmsg) {
        e.textContent = result.errmsg;
        $(e).show();
        e.className = "ratefail";
    }
    else {
        e.textContent = "评分成功！";
        $(e).show();
        e.className = "ratesuccess";
        updaterate(result.rating);
    }
}
function selectText(element) {
    var doc = document
        , text = element
        , range, selection
    ;
    if (doc.body.createTextRange) { //ms
        range = doc.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) { //all others
        selection = window.getSelection();
        range = doc.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}