﻿var btnclicked = false;
var btn;
var btnidd;
function startsubmit(btnref) {
    btnidd = btnref;
    btn = $(btnref);
    if (btnclicked)
        return false;
    btnclicked = true;
    btn.attr('disabled', 'disabled');
    $("<img id='loadinggif' src='~/Images/loading2.gif />").insertAfter(btn);
    //setTimeout(endsubmit, 5000);
    return true;
}
function endsubmit() {
    if (btn) {
        btn.removeAttr('disabled');
        $('#loadinggif').remove();
    }
    btnclicked = false;
}