﻿function setCookie(c_name, value) {
    if (document.cookie.indexOf(c_name + '=') >= 0) {
        var zero = new Date(0);
        document.cookie = c_name + '= ; expires=' + zero.toUTCString();
    }
    var d = new Date();
    d.setTime(d.getTime() + 31536000000);
    document.cookie = c_name + '=' + value + '; expires=' + d.toUTCString();
}
function getCookie(c_name) {
    var cookie = document.cookie;
    var spos = cookie.indexOf(c_name + '=');
    if (spos < 0)
        return null;
    else {
        spos = cookie.indexOf('=', spos) + 1;
        var epos = cookie.indexOf(";", spos);
        if (epos < 0)
            epos = cookie.length;
        return cookie.substring(spos, epos);
    }
}