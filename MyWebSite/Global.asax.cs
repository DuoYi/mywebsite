﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Data.Entity;
using MyMVCWeb.Models;
using WebMatrix.WebData;
using System.Configuration;
using FluentScheduler;

namespace MyMVCWeb
{
    // 注意: 有关启用 IIS6 或 IIS7 经典模式的说明，
    // 请访问 http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            //init database
            Database.SetInitializer(new BlogDBinit());
            Database.SetInitializer(new UserDBinit());
            using (var dbdata = new BlogContext())
            {
                dbdata.Database.Initialize(false);
            }
            using (var dbuser = new UsersContext())
            {
                dbuser.Database.Initialize(false);
            }
            
            if(!WebSecurity.Initialized)
                WebSecurity.InitializeDatabaseConnection("MyMvcWebUser", "UserProfile", "UserId", "UserName", autoCreateTables: true);
            if (!Roles.RoleExists("Banned"))
            {
                Roles.CreateRole("Banned");
            }
            //init parameters
            string intervalstr = ConfigurationManager.AppSettings.Get("UpdateInterval");
            int interval;
            if(!int.TryParse(intervalstr, out interval)){
                interval = 5; //5 minutes
            }
            intervalstr = ConfigurationManager.AppSettings.Get("RankingSize");
            int ranksize;
            if (!int.TryParse(intervalstr, out ranksize))
            {
                ranksize = 5; //5 items
            }
            TaskManager.Initialize(new MyMVCWeb.App_Start.SchedulerConfig(interval,ranksize));

            ReadWebSettings();
            ReadWebData();


        }

        protected void ReadWebSettings()
        {
            string configstr = ConfigurationManager.AppSettings.Get("ExpAddOnPass");
            int configval;
            if (!int.TryParse(configstr, out configval))
            {
                configval = 10;
            }
            Application["ExpAddOnPass"] = configval;
            configstr = ConfigurationManager.AppSettings.Get("ExpAddOnDay");
            bool isgood = true;
            List<int> ExpAddOnDay = null;
            if (configstr != null)
            {
                string[] days = configstr.Split(',');
                ExpAddOnDay = new List<int>(days.Length);
                foreach (var day in days)
                {
                    int val;
                    if (!int.TryParse(day, out val))
                    {
                        isgood = false;
                        break;
                    }
                    ExpAddOnDay.Add(val);
                }
            }
            if (configstr == null || !isgood)
            {
                ExpAddOnDay = new List<int> { 1, 2, 3, 4, 5 };
            }
            Application["ExpAddOnDay"] = ExpAddOnDay;

            string[] configkeys = new string[] { "ListPagesize", "HomePagesize", "AuditPagesize", "UserPagesize", "ReplyPagesize", "MsgPagesize" };
            foreach (var key in configkeys)
            {
                configstr = ConfigurationManager.AppSettings.Get(key);
                if (int.TryParse(configstr, out configval) && configval > 0)
                {
                    Application[key] = configval;
                }
            }
        }
        protected void ReadWebData()
        {
            string path = Server.MapPath("~/App_Data/featuredBlogId.json");
            if (System.IO.File.Exists(path))
            {
                string featuredata = System.IO.File.ReadAllText(path);
                var features = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(featuredata);
                if (features != null && features.Count > 0)
                {
                    Application["FeaturedBlogId"] = features;
                }
            }
        }


        protected void Session_Start()
        {
            Session["captchaid"] = 1;
            Session["captchaerror"] = 0;
        }

        protected void Application_AuthenticateRequest()
        {
            if (User != null)
            {
                if (Roles.IsUserInRole("Banned"))
                {
                    WebSecurity.Logout();
                }
            }
        }
    }
}