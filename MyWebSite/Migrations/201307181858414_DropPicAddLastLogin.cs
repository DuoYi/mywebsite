namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropPicAddLastLogin : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserProfile", "avatar_PicID", "dbo.Pictures");
            DropIndex("dbo.UserProfile", new[] { "avatar_PicID" });
            AddColumn("dbo.UserProfile", "LastLoginDate", c => c.DateTime(nullable: false, defaultValueSql:"GetDate()"));
            DropColumn("dbo.UserProfile", "avatar_PicID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfile", "avatar_PicID", c => c.Int());
            DropColumn("dbo.UserProfile", "LastLoginDate");
            CreateIndex("dbo.UserProfile", "avatar_PicID");
            AddForeignKey("dbo.UserProfile", "avatar_PicID", "dbo.Pictures", "PicID");
        }
    }
}
