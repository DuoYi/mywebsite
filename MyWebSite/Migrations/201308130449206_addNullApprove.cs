namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNullApprove : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Blogs", "isApproved", c => c.Boolean(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Blogs", "isApproved", c => c.Boolean(nullable: false));
        }
    }
}
