namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTopicOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogsInTopics", "BlogOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BlogsInTopics", "BlogOrder");
        }
    }
}
