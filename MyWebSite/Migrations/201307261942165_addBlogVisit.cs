namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBlogVisit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "BlogVisit", c => c.Long(nullable: false,defaultValue:0));
            AlterColumn("dbo.Categories", "Description", c => c.String(maxLength: 256));
            AlterColumn("dbo.Blogs", "ImagePath", c => c.String(maxLength: 512));
            AlterColumn("dbo.Blogs", "Author", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Posts", "Author", c => c.String(maxLength: 30));
            AlterColumn("dbo.Tags", "TagName", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "TagName", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "Author", c => c.String());
            AlterColumn("dbo.Blogs", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Blogs", "ImagePath", c => c.String());
            AlterColumn("dbo.Categories", "Description", c => c.String());
            DropColumn("dbo.Blogs", "BlogVisit");
        }
    }
}
