namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addExp : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExperienceTables",
                c => new
                    {
                        Level = c.Int(nullable: false, identity: true),
                        ExperienceStart = c.Int(nullable: false),
                        ExperienceEnd = c.Int(nullable: false),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Level);
            
            AddColumn("dbo.UserProfile", "Experience", c => c.Int(nullable: false,defaultValue: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "Experience");
            DropTable("dbo.ExperienceTables");
        }
    }
}
