namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 20),
                        UserComment = c.String(maxLength: 200),
                        Email = c.String(nullable: false, maxLength: 50),
                        LastLoginDate = c.DateTime(nullable: false),
                        LastLoginIP = c.String(maxLength: 80),
                        Points = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MsgId = c.Int(nullable: false, identity: true),
                        Sender = c.String(nullable: false, maxLength: 30),
                        Recipient = c.String(nullable: false, maxLength: 30),
                        MsgDate = c.DateTime(nullable: false),
                        MsgContent = c.String(nullable: false),
                        MsgTitle = c.String(maxLength: 80),
                        IsRead = c.Boolean(nullable: false),
                        IsSenderDelete = c.Boolean(nullable: false),
                        IsRecipientDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.MsgId);
            
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        PicID = c.Int(nullable: false, identity: true),
                        PicType = c.String(nullable: false, maxLength: 50),
                        PicName = c.String(nullable: false, maxLength: 256),
                        Data = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.PicID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pictures");
            DropTable("dbo.Messages");
            DropTable("dbo.UserProfile");
        }
    }
}
