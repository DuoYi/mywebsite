namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTopicPost : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TopicPosts",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        PostDate = c.DateTime(nullable: false),
                        Author = c.String(maxLength: 30),
                        Content = c.String(nullable: false),
                        topic_TopicID = c.Int(),
                    })
                .PrimaryKey(t => t.PostID)
                .ForeignKey("dbo.Topics", t => t.topic_TopicID)
                .Index(t => t.topic_TopicID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TopicPosts", new[] { "topic_TopicID" });
            DropForeignKey("dbo.TopicPosts", "topic_TopicID", "dbo.Topics");
            DropTable("dbo.TopicPosts");
        }
    }
}
