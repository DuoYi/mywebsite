namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addOptions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserOptions",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        sendNoticeForNewReply = c.Boolean(nullable: false, defaultValue: true),
                        sendNoticeForNewPostReply = c.Boolean(nullable: false, defaultValue: true),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.UserProfile", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserOptions", new[] { "UserId" });
            DropForeignKey("dbo.UserOptions", "UserId", "dbo.UserProfile");
            DropTable("dbo.UserOptions");
        }
    }
}
