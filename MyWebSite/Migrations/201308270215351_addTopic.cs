namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTopic : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Topics",
                c => new
                    {
                        TopicID = c.Int(nullable: false, identity: true),
                        TopicTitle = c.String(nullable: false, maxLength: 80),
                        Content = c.String(nullable: false),
                        ImagePath = c.String(maxLength: 512),
                        isLocalImg = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Author = c.String(nullable: false, maxLength: 30),
                        TopicVisit = c.Long(nullable: false),
                        CategoryID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TopicID)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: false)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: false)
                .Index(t => t.TagID)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.BlogsInTopics",
                c => new
                    {
                        TopicID = c.Int(nullable: false),
                        BlogID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TopicID, t.BlogID })
                .ForeignKey("dbo.Topics", t => t.TopicID, cascadeDelete: true)
                .ForeignKey("dbo.Blogs", t => t.BlogID, cascadeDelete: true)
                .Index(t => t.TopicID)
                .Index(t => t.BlogID);
            
            AlterColumn("dbo.Blogs", "ImagePath", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            DropIndex("dbo.BlogsInTopics", new[] { "BlogID" });
            DropIndex("dbo.BlogsInTopics", new[] { "TopicID" });
            DropIndex("dbo.Topics", new[] { "CategoryID" });
            DropIndex("dbo.Topics", new[] { "TagID" });
            DropForeignKey("dbo.BlogsInTopics", "BlogID", "dbo.Blogs");
            DropForeignKey("dbo.BlogsInTopics", "TopicID", "dbo.Topics");
            DropForeignKey("dbo.Topics", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.Topics", "TagID", "dbo.Tags");
            AlterColumn("dbo.Blogs", "ImagePath", c => c.String(maxLength: 512));
            DropTable("dbo.BlogsInTopics");
            DropTable("dbo.Topics");
        }
    }
}
