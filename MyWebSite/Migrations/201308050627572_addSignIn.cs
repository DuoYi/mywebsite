namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSignIn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "Level", c => c.Int(nullable: false, defaultValue:1));
            AddColumn("dbo.UserProfile", "LastSignDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.UserProfile", "ConsecutiveSign", c => c.Int(nullable: false, defaultValue:0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "ConsecutiveSign");
            DropColumn("dbo.UserProfile", "LastSignDate");
            DropColumn("dbo.UserProfile", "Level");
        }
    }
}
