namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPostReply : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Replies",
                c => new
                    {
                        ReplyId = c.Int(nullable: false, identity: true),
                        ReplyDate = c.DateTime(nullable: false),
                        Author = c.String(maxLength: 30),
                        Content = c.String(nullable: false),
                        PostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ReplyId)
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Replies", new[] { "PostId" });
            DropForeignKey("dbo.Replies", "PostId", "dbo.Posts");
            DropTable("dbo.Replies");
        }
    }
}
