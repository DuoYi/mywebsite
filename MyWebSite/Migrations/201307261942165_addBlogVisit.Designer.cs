// <auto-generated />
namespace MyMVCWeb.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class addBlogVisit : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addBlogVisit));
        
        string IMigrationMetadata.Id
        {
            get { return "201307261942165_addBlogVisit"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
