namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAds : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Advertisments",
                c => new
                    {
                        AdID = c.Int(nullable: false, identity: true),
                        AdUrl = c.String(nullable: false, maxLength: 1000),
                        ImgUrl = c.String(maxLength: 1000),
                        AdTitle = c.String(nullable: true, maxLength: 100),
                        ExposeCount = c.Long(nullable: false,defaultValue:0),
                        ClickCount = c.Int(nullable: false, defaultValue: 0),
                    })
                .PrimaryKey(t => t.AdID);
            CreateIndex("dbo.Advertisments", "ClickCount", false);
        }
        
        public override void Down()
        {
            DropTable("dbo.Advertisments");
        }
    }
}
