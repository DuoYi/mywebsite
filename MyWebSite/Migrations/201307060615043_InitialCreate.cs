namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        UserComment = c.String(),
                        Email = c.String(),
                        avatar_PicID = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Pictures", t => t.avatar_PicID)
                .Index(t => t.avatar_PicID);
            
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        PicID = c.Int(nullable: false, identity: true),
                        PicType = c.String(nullable: false),
                        PicName = c.String(nullable: false),
                        Data = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.PicID);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MsgId = c.Int(nullable: false, identity: true),
                        Sender = c.String(nullable: false),
                        Recipient = c.String(nullable: false),
                        MsgDate = c.DateTime(nullable: false),
                        MsgContent = c.String(nullable: false),
                        MsgTitle = c.String(maxLength: 80),
                        IsRead = c.Boolean(nullable: false),
                        IsSenderDelete = c.Boolean(nullable: false),
                        IsRecipientDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.MsgId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserProfile", new[] { "avatar_PicID" });
            DropForeignKey("dbo.UserProfile", "avatar_PicID", "dbo.Pictures");
            DropTable("dbo.Messages");
            DropTable("dbo.Pictures");
            DropTable("dbo.UserProfile");
        }
    }
}
