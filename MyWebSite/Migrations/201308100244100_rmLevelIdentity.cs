namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rmLevelIdentity : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ExperienceTables", "Level", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ExperienceTables", "Level", c => c.Int(nullable: false, identity: true));
        }
    }
}
