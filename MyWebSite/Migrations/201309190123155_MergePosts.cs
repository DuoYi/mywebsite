namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MergePosts : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TopicPosts", "topic_TopicID", "dbo.Topics");
            DropIndex("dbo.TopicPosts", new[] { "topic_TopicID" });
            RenameColumn(table: "dbo.Posts", name: "blog_BlogID", newName: "BlogId");
            AddColumn("dbo.Posts", "TopicId", c => c.Int(nullable:true));
            AddForeignKey("dbo.Posts", "TopicId", "dbo.Topics", "TopicID");
            CreateIndex("dbo.Posts", "TopicId");
            DropTable("dbo.TopicPosts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TopicPosts",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        PostDate = c.DateTime(nullable: false),
                        Author = c.String(maxLength: 30),
                        Content = c.String(nullable: false),
                        topic_TopicID = c.Int(),
                    })
                .PrimaryKey(t => t.PostID);
            
            DropIndex("dbo.Posts", new[] { "TopicId" });
            DropForeignKey("dbo.Posts", "TopicId", "dbo.Topics");
            DropColumn("dbo.Posts", "TopicId");
            RenameColumn(table: "dbo.Posts", name: "BlogId", newName: "blog_BlogID");
            CreateIndex("dbo.TopicPosts", "topic_TopicID");
            AddForeignKey("dbo.TopicPosts", "topic_TopicID", "dbo.Topics", "TopicID");
        }
    }
}
