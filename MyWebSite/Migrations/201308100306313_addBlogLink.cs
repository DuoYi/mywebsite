namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBlogLink : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "Links", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Blogs", "Links");
        }
    }
}
