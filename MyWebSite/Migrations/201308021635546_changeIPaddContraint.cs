namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeIPaddContraint : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Ratings", "ip", c => c.String(maxLength: 50));
            Sql("ALTER TABLE dbo.Ratings ADD CONSTRAINT [UNQ_IPTIME] UNIQUE NONCLUSTERED ([ip] ASC, [ratetime] ASC, [BlogID] ASC)");
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Ratings", "ip", c => c.String());
        }
    }
}
