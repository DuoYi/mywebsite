namespace MyMVCWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtags : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        TagName = c.String(nullable: false),
                        TagVisit = c.Long(nullable: false,defaultValue:0),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.TagsInBlogs",
                c => new
                    {
                        BlogID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BlogID, t.TagID })
                .ForeignKey("dbo.Blogs", t => t.BlogID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.BlogID)
                .Index(t => t.TagID);
            
            DropColumn("dbo.Blogs", "blogTags");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Blogs", "blogTags", c => c.String());
            DropIndex("dbo.TagsInBlogs", new[] { "TagID" });
            DropIndex("dbo.TagsInBlogs", new[] { "BlogID" });
            DropForeignKey("dbo.TagsInBlogs", "TagID", "dbo.Tags");
            DropForeignKey("dbo.TagsInBlogs", "BlogID", "dbo.Blogs");
            DropTable("dbo.TagsInBlogs");
            DropTable("dbo.Tags");
        }
    }
}
