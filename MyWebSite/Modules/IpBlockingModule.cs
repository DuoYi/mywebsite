#region Using

using System;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;

#endregion

/// <summary>
/// Block the response to certain IP addresses
/// </summary>
public sealed class IpBlockingModule : IHttpModule
{

  #region IHttpModule Members

  void IHttpModule.Dispose()
  {
    // Nothing to dispose; 
  }

  void IHttpModule.Init(HttpApplication context)
  {
    context.BeginRequest += new EventHandler(context_BeginRequest);
  }

  #endregion

  /// <summary>
  /// Checks the requesting IP address in the collection
  /// and block the response if it's on the list.
  /// </summary>
  private void context_BeginRequest(object sender, EventArgs e)
  {
    string ip = HttpContext.Current.Request.UserHostAddress;
    if (_IpAddresses.Contains(ip))
    {
      HttpContext.Current.Response.StatusCode = 403;
      HttpContext.Current.Response.End();
    }
    else
    {
        foreach (string s in _IpAddresses)
        {
            string checkip;
            if (s.EndsWith(".*.*"))
            {
                checkip = ip.Substring(0, ip.LastIndexOf("."));
                checkip = checkip.Substring(0, checkip.LastIndexOf("."));
                string s2 = s.Substring(0, s.LastIndexOf(".*.*"));
                if (checkip == s2)
                {
                    HttpContext.Current.Response.StatusCode = 403;
                    HttpContext.Current.Response.End();
                }
            }
            else if (s.EndsWith(".*"))
            {
                checkip = ip.Substring(0, ip.LastIndexOf("."));
                string s3 = s.Substring(0, s.LastIndexOf(".*"));
                if (checkip == s3)
                {
                    HttpContext.Current.Response.StatusCode = 403;
                    HttpContext.Current.Response.End();
                }
            }
        }
    } 
  }

  private static StringCollection _IpAddresses = FillBlockedIps();

  /// <summary>
  /// Retrieves the IP addresses from the web.config
  /// and adds them to a StringCollection.
  /// </summary>
  /// <returns>A StringCollection of IP addresses.</returns>
  private static StringCollection FillBlockedIps()
  {
    StringCollection col = new StringCollection();
    string raw = ConfigurationManager.AppSettings.Get("blockip");

    foreach (string ip in raw.Split(new char[]{';',',',' ','��'}, StringSplitOptions.RemoveEmptyEntries))
    {
      col.Add(ip.Trim());
    }

    return col;
  }

}
