﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Web;

public sealed class CustomBlockingModule : IHttpModule
{
    #region IHttpModule Members

    void IHttpModule.Dispose()
    {
        // Nothing to dispose; 
    }

    void IHttpModule.Init(HttpApplication context)
    {
        context.BeginRequest += new EventHandler(context_BeginRequest);
    }

    #endregion
    private static ConcurrentQueue<string> _Banned = new ConcurrentQueue<string>();
    private static Timer _BannedTimer = CreateBanningTimer();
    private const int RELEASE_INTERVAL = 30 * 60 * 1000; // 30 minutes

    private void context_BeginRequest(object sender, EventArgs e)
    {
        string ip = HttpContext.Current.Request.UserHostAddress;
        if (_Banned.Contains(ip))
        {
            HttpContext.Current.Response.StatusCode = 403;
            HttpContext.Current.Response.StatusDescription = "You entered the wrong Captcha too many times and are banned for 30 min.";
            HttpContext.Current.Response.End();
        }

        UpdateBanList();
    }

    /// <summary>
    /// Checks the requesting IP address in the collection
    /// and bannes the IP if required.
    /// </summary>
    private static void UpdateBanList()
    {
        string iptoban = HttpContext.Current.Application["iptoban"] as string;
        if (iptoban == null)
        {
            return;
        }
        else
        {
            _Banned.Enqueue(iptoban);
            HttpContext.Current.Application["iptoban"] = null;
        }
        if (_Banned.IsEmpty)
        {
            _BannedTimer.Enabled = false;
        }
        else if (!_BannedTimer.Enabled)
        {
            _BannedTimer.Enabled = true;
        }
    }

    #region Timers

    /// <summary>
    /// Creates the timer that removes 1 banned IP address
    /// everytime the timer is elapsed.
    /// </summary>
    /// <returns></returns>
    private static Timer CreateBanningTimer()
    {
        Timer timer = GetTimer(RELEASE_INTERVAL);
        timer.Elapsed += delegate { string v; _Banned.TryDequeue(out v); };
        return timer;
    }

    /// <summary>
    /// Creates a simple timer instance and starts it.
    /// </summary>
    /// <param name="interval">The interval in milliseconds.</param>
    private static Timer GetTimer(int interval)
    {
        Timer timer = new Timer();
        timer.Interval = interval;
        timer.Enabled = false;
        return timer;
    }

    #endregion

}
