﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyMVCWeb.Models
{
    public class BlogDisplay
    {
        public Blog blog { get; set; }
        public IEnumerable<Tag> tag { get; set; }
    }
    public class HeaderDisplay
    {
        public Category category { get; set; }
        public int newItems { get; set; }
    }

    public class BlogEdit
    {
        [Required(ErrorMessage = "请输入标题"), StringLength(80, ErrorMessage = "标题不得超过80个字符"), Display(Name = "标题")]
        public string BlogTitle { get; set; }

        [Required(ErrorMessage = "请输入内容"), StringLength(int.MaxValue), Display(Name = "内容"), DataType(DataType.MultilineText)]
        public string Content { get; set; }
        [MaxLength(512)]
        public string ImagePath { get; set; }
        [Required]
        public int CategoryID { get; set; }

        public bool islocalimg { get; set; }
        [MaxLength(100)]
        public string BlogTags { get; set; }

        public BlogLink[] BlogPasswords { get; set; }

        public BlogLink[] BlogLinks { get; set; }

        public BlogEdit(Blog blog)
        {
            BlogTitle = blog.BlogTitle;
            Content = blog.Content;
            ImagePath = blog.ImagePath;
            CategoryID = blog.CategoryID;
            islocalimg = blog.isLocalImg;
            string[] tmp = BlogUtil.GetTagNamesInBlog(blog.BlogID);
            if (tmp != null && tmp.Length != 0)
                BlogTags = string.Join(", ", tmp);
            BlogLinks = BlogUtil.getBlogLink(blog.Links);
        }
        public BlogEdit()
        {

        }
    }

    public class Ranking
    {
        public int blogId { get; set; }
        public string blogTitle { get; set; }
        public string blogThumb { get; set; }
        public bool isLocalImg { get; set; }
        public int rating { get; set; }
    }

    public class BlogLink
    {
        public string name { get; set; }
        public string url { get; set; }
    }

    public class TopicEdit
    {
        [Required(ErrorMessage = "请输入标题"), StringLength(80, ErrorMessage = "标题不得超过80个字符"), Display(Name = "标题")]
        public string TopicTitle { get; set; }

        [Required(ErrorMessage = "请输入内容"), DataType(DataType.MultilineText)]
        public string Content { get; set; }
        [ValidateFile(4194304, ErrorMessage="封面必须是图片，且不得超过4MB")]
        public HttpPostedFileBase TopicImage { get; set; }

        public string ImagePath { get; set; }
        public bool isLocalImg { get; set; }

        [Required]
        public int CategoryID { get; set; }

        [Required(ErrorMessage = "请添加一个标签")]
        public string TagName { get; set; }

        [Required(ErrorMessage = "请添加资源")]
        public List<int> blogIDs { get; set; }

        public TopicEdit(Topic topic)
        {
            TopicTitle = topic.TopicTitle;
            Content = topic.Content;
            ImagePath = topic.ImagePath;
            CategoryID = topic.CategoryID;
            isLocalImg = topic.isLocalImg;
            TagName = topic.tag.TagName;
            blogIDs = BlogUtil.getBlogIDinTopic(topic.TopicID);
        }
        public TopicEdit()
        {

        }
    }

    public class BlogWithRating
    {
        public Blog blog { get; set; }
        public int rating { get; set; }
    }

    public class TopicDisplay
    {
        public Topic topic { get; set; }
        public List<Blog> blogs { get; set; }
    }

    public class SearchDisplay
    {
        public int itemType { get; set; }
        public int itemID { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public string Image { get; set; }
        public bool isLocalImage { get; set; }
        public DateTime Date { get; set; }
        public long Visit { get; set; }
        public int Posts { get; set; }
        public DateTime? LastPost { get; set; }
    }
}