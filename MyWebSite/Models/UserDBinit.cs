﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using WebMatrix.WebData;
using System.Web.Security;

namespace MyMVCWeb.Models
{
    public class UserDBinit : CreateDatabaseIfNotExists<UsersContext>
    {
        protected override void Seed(UsersContext context)
        {
            WebSecurity.InitializeDatabaseConnection("MyMvcWebUser", "UserProfile", "UserId", "UserName", autoCreateTables: true);

            if (!Roles.RoleExists("Administrator"))
            {
                Roles.CreateRole("Administrator");
            }
            if (!WebSecurity.UserExists("admin"))
            {
                WebSecurity.CreateUserAndAccount("admin", "admin", new { Email = "yiduo208@gmail.com", Points=100, LastLoginDate = DateTime.Now, Level=0, ConsecutiveSign=0,Experience=1,LastSignDate=new DateTime(1900,1,1) });
                Roles.AddUserToRole("admin", "Administrator");
            }
            if (!Roles.RoleExists("Writers"))
            {
                Roles.CreateRole("Writers");
            }
            if (!Roles.RoleExists("Moderator"))
            {
                Roles.CreateRole("Moderator");
            }
            if (!Roles.RoleExists("Banned"))
            {
                Roles.CreateRole("Banned");
            }

            GetExpTableSample().ForEach(e => context.ExpTable.Add(e));
            context.Database.ExecuteSqlCommand("Insert Into dbo.ExperienceTables (Level, ExperienceStart,ExperienceEnd,Title) values (0,0,0,'缺省')");
            context.Database.ExecuteSqlCommand("Insert Into dbo.ExperienceTables (Level, ExperienceStart,ExperienceEnd,Title) values (-1,-1,-1,'小黑屋')");
            context.Database.ExecuteSqlCommand("Insert Into dbo.ExperienceTables (Level, ExperienceStart,ExperienceEnd,Title) values (99,0,0,'管理员')");
            context.SaveChanges();
        }

        private List<ExperienceTable> GetExpTableSample(){
            return new List<ExperienceTable>(){
                new ExperienceTable{ ExperienceStart=21, ExperienceEnd=100, Level=2, Title="绅士"},
                new ExperienceTable{ ExperienceStart=1, ExperienceEnd=20, Level=1, Title="路人"},
            };
        }
    }
}