﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyMVCWeb.Models
{
    public static class TopicUtil
    {
        public static int getBlogCount(int id){
            using (var db = new BlogContext())
            {
                return db.BlogsInTopics.Where(t => t.TopicID == id).Count();
            }
        }
        public static string getBlogTitle(int id)
        {
            using (var db = new BlogContext())
            {
                Blog b = db.Blogs.Find(id);
                if (b == null || id <=0)
                    return null;
                else
                    return b.BlogTitle;
            }
        }
    }
}