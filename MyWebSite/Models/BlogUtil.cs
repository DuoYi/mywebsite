﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Net.Mail;

namespace MyMVCWeb.Models
{
    public enum NoticeType
    {
        default_notice=0,
        newpost,
        newreply,
        delpost,
        delreply,
        delblog
    }
    static public class BlogUtil
    {
        static public int AddBlog(string title, string content, int blogCategory, string ImagePath, string author, bool approve, bool isLocalimg, IEnumerable<BlogLink> links)
        {
            var newBlog = new Blog();
            newBlog.BlogTitle = title;
            newBlog.Content = content;
            newBlog.BlogDate = DateTime.Now;
            newBlog.ImagePath = ImagePath;
            newBlog.CategoryID = blogCategory;
            newBlog.Author = author;
            newBlog.isApproved = approve? true:(bool?)null;
            newBlog.isLocalImg = isLocalimg;
            newBlog.Links = Newtonsoft.Json.JsonConvert.SerializeObject(links);
            // Get DB context.
            using (BlogContext _db = new BlogContext())
            {

                // Add product to DB.
                _db.Blogs.Add(newBlog);
                _db.SaveChanges();
            }
            // Success.
            return newBlog.BlogID;
        }

        static public int AddPost(int blogid, string author, string content)
        {
            Post p = new Post();
            p.Author = author;
            p.Content = content;
            p.PostDate = DateTime.Now;
            using (BlogContext _db = new BlogContext())
            {
                Blog b = _db.Blogs.Find(blogid);
                if (blogid > 0)
                {
                    using (UsersContext db = new UsersContext())
                    {
                        UserOption o = db.UserProfiles.Single(u => u.UserName == b.Author).option;
                        if (o == null || o.sendNoticeForNewReply == true)
                        {
                            SendNoticeMsg(b.Author, NoticeType.newpost, author, b.BlogTitle, "/gm" + b.BlogID);
                        }
                    }
                }
                b.posts.Add(p);
                _db.SaveChanges();
            }
            return p.PostId;
        }
        static public int AddTopicPost(int topicid, string author, string content)
        {
            var p = new Post();
            p.Author = author;
            p.Content = content;
            p.PostDate = DateTime.Now;
            using (BlogContext _db = new BlogContext())
            {
                Topic t = _db.Topics.Find(topicid);
                if (topicid > 0)
                {
                    using (UsersContext db = new UsersContext())
                    {
                        UserOption o = db.UserProfiles.Single(u => u.UserName == t.Author).option;
                        if (o == null || o.sendNoticeForNewReply == true)
                        {
                            SendNoticeMsg(t.Author, NoticeType.newpost, author, t.TopicTitle, "/gmt" + t.TopicID);
                        }
                    }
                }
                t.Posts.Add(p);
                _db.SaveChanges();
            }
            return p.PostId;
        }

        static public int AddPostReply(int id, string author, string content)
        {
            var r = new Reply();
            r.Author = author;
            r.Content = content;
            r.PostId = id;
            r.ReplyDate = DateTime.Now;
            using (BlogContext _db = new BlogContext())
            {
                Post p = _db.Posts.Find(id);
                if (id > 0)
                {
                    string noticeuser = p.Author;
                    int spos = content.IndexOf("回复 ");
                    if (spos > 0 && spos < 10)
                    {
                        spos += 3;
                        int epos = content.IndexOfAny(":：".ToCharArray(), spos);
                        if (epos > 0)
                        {
                            noticeuser = content.Substring(spos, epos - spos).Trim();
                        }
                    }
                    using (UsersContext db = new UsersContext())
                    {
                        var user =db.UserProfiles.SingleOrDefault(u => u.UserName == noticeuser);
                        if(user != null){
                            UserOption o = user.option;
                            if (o == null || o.sendNoticeForNewPostReply == true)
                            {
                                SendNoticeMsg(noticeuser, NoticeType.newreply, author,
                                    p.blog != null ? p.blog.BlogTitle : p.topic.TopicTitle,
                                    p.blog != null ? "/gm" + p.BlogId : "/gmt" + p.TopicId);
                            }
                        }
                    }
                }
                p.repies.Add(r);
                _db.SaveChanges();
            }
            return r.ReplyId;
        }

        static public int AddMsg(string author, string recipient, string title, string content, bool senderdel=false)
        {
            Message m = new Message();
            m.Sender = author;
            m.Recipient = recipient;
            m.MsgTitle = title;
            m.MsgContent = content;
            m.MsgDate = DateTime.Now;
            if (senderdel)
            {
                m.IsSenderDelete = true;
            }
            using (UsersContext _db = new UsersContext())
            {
                _db.Messages.Add(m);
                _db.SaveChanges();
            }
            return m.MsgId;
        }

        public static int SendNoticeMsg(string noticeuser, NoticeType type, string actor, string content=null, string url=null)
        {
            if (noticeuser == actor)
            {
                return 0;
            }
            switch (type)
            {
                case NoticeType.newpost:
                    return AddMsg("admin", noticeuser, "新回复通知", string.Format("<a href='/User/{0}'>{0}</a> 回复了您的投稿：<a href='{2}'>{1}</a>", actor, content, url), true);
                case NoticeType.newreply:
                    return AddMsg("admin", noticeuser, "新回复通知", string.Format("<a href='/User/{0}'>{0}</a> 在投稿<a href='{2}'>{1}</a>中回复了您的评论", actor, content, url), true);
                case NoticeType.delpost:
                    return AddMsg(actor, noticeuser, "删除通知", string.Format("您在投稿<a href='{1}'>{0}</a>中的评论已被管理员删除", content, url));
                case NoticeType.delreply:
                    return AddMsg(actor, noticeuser, "删除通知", string.Format("您在投稿<a href='{1}'>{0}</a>中的回复已被管理员删除", content, url));
                case NoticeType.delblog:
                    return AddMsg(actor, noticeuser, "删除通知", string.Format("您的投稿 {1} 已被管理员删除<br>原因：<br>{0}", content, url));
                //case NoticeType.editpost:
                //    return AddMsg(actor, noticeuser, "编辑通知", string.Format("您在投稿<a href='{1}'>{0}</a>中的评论已被管理员编辑", content, url));
                //case NoticeType.editblog:
                //    return AddMsg(actor, noticeuser, "编辑通知", string.Format("您的投稿<a href='{1}'>{0}</a>已被管理员编辑", content, url));
                default:
                    return 0;
            }
        }

        public static string categoryName(int id)
        {
            using (var db = new BlogContext())
                return db.Categories.SingleOrDefault(c => c.CategoryID == id).CategoryName;
        }
        public static string categoryDesc(int id)
        {
            using (var db = new BlogContext())
                return db.Categories.SingleOrDefault(c => c.CategoryID == id).Description;
        }

        public static string getFirstLine(string content, int maxlength, bool removetags = false)
        {
            //int i = content.IndexOf('<');
            //if (i<0)//no tags?
            //    i = 20;
            //else{
            content = System.Text.RegularExpressions.Regex.Replace(content, "<[/]?img[^>]*>", string.Empty);
            content = System.Text.RegularExpressions.Regex.Replace(content, @"\[img[^\]]+\]", string.Empty);
            content = content.Replace("&nbsp;", " ");
            content = content.Replace("&hellip;", "…");
            int i = content.IndexOf("\r\n");
            //if (i <= 0 || i >= maxlength)
            //{
            //    i = content.ToLower().IndexOf("<br");
            //}
            if (i <= 0 || i >= maxlength)
            {
                string s = System.Text.RegularExpressions.Regex.Replace(content, "<[^>]*>", string.Empty);
                i = s.Length < maxlength ? s.Length : maxlength;
                return s.Substring(0, i);
            }
            else
            {
                content = content.Substring(0, i);
                if(removetags)
                    content = System.Text.RegularExpressions.Regex.Replace(content, "<[^>]*>", string.Empty);
                return content;
            }
        }
        public static string removeAllTags(string content)
        {
            return System.Text.RegularExpressions.Regex.Replace(content, "<[^>]*>", string.Empty);
        }
        public static string getRating(string id)
        {
            return getRating(int.Parse(id));
        }

        public static string getRating(int id)
        {
            double? avgrate;
            int sum;
            int count;
            using (var db = new BlogContext())
            {
                var rates = db.Ratings.Where(r => r.BlogID == id);
                count = rates.Count();
                if (count == 0)
                {
                    return "还没有人评分";
                }
                avgrate = rates.Average(r => r.value);
                sum = rates.Sum(r => r.value);
            }
            return String.Format("{2}分（平均{0:0.##}），（共{1}次评分）", avgrate, count, sum);
        }

        public static int? getTotalRating(int id)
        {
            using (var db = new BlogContext())
            {
                var rates = db.Ratings.Where(r => r.BlogID == id);
                if (rates == null || rates.Count() == 0)
                    return 0;
                return rates.Sum(r => r.value);
            }
        }

        public static double? getAvarageRating(int id)
        {
            using (var db = new BlogContext())
            {
                var rates = db.Ratings.Where(r => r.BlogID == id);
                if (rates == null || rates.Count() == 0)
                    return 0;
                return rates.Average(r => r.value);
            }
        }

        public static BlogLink[] getBlogLink(string links)
        {
            return links == null ? null : Newtonsoft.Json.JsonConvert.DeserializeObject<BlogLink[]>(links);
        }

        public static void deleteBlog(int id)
        {
            var _db = new BlogContext();
            Blog b = _db.Blogs.Single(bb => bb.BlogID == id);
            if (b == null)
                return;
            DelTagsInBlog(b.BlogID);
            if (b.posts.Count != 0)
                b.posts.RemoveRange(0, b.posts.Count);

            IEnumerable<string> name = b.ImagePath.Split(';');
            try
            {
                if (b.isLocalImg)
                    removeFiles(name,true);
            }
            finally
            {
                _db.Blogs.Remove(b);
                _db.SaveChanges();
            }
        }

        public static bool checkContentTags(string content)
        {
            Regex r = new Regex(@"</?[ ]*(script|embed|object|frameset|frame|iframe|meta|link|style|html)(.|\n)*?>",RegexOptions.IgnoreCase);
            if (r.IsMatch(content))
            {
                return false;
            }
            return true;
        }
        //get first img from content
        public static string getFirstImg(string content)
        {
            string result = null;
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            Match matchImgSrc = Regex.Match(content, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            result = matchImgSrc.Groups[1].Value;

            return result;
        }
        //get first img path from ImgPath string
        public static string firstImgPath(string path)
        {
            if (path == null)
                return null;
            int s = path.IndexOf(';');
            if (s > 0)
            {
                return path.Substring(0, s);
            }
            return path;
        }
        //get rest img path from ImgPath string and form <a><img></a>tags
        public static string getRestImgTags(string path, bool readmode)
        {
            if (path == null)
                return null;
            string output = "";
            string format = readmode ? "<a href='/Upload/{0}' rel='lightbox[g]'><img id='imgdiv{1}' src='/Upload/{0}' class='thumbs'/></a>"
                : "<div id='imgdiv{1}' class='editthumbs'><a title='主预览图' onclick='set({1});' class='set'></a><a title='删除' onclick='del({1});' class='del'></a><a title='嵌入' onclick='insertAttach({1});' class='ins'></a><img id='thumb{1}' src='/Upload/{0}' class='thumbs' /></div>";
            int s = path.IndexOf(';');
            int i = 1;
            if (s > 0)
            {
                string[] strs = path.Substring(s + 1).Split(';');
                foreach (string str in strs)
                {
                    output += string.Format(format, str, i++);
                }
            }
            return output;
        }

        public static string replaceImgPlaceholder(string content, string imgpath)
        {
            string[] imgs = imgpath.Split(';');
            string format = "<a href='/Upload/{0}' rel='lightbox'><img src='/Upload/{0}' class='uimg'/></a>";
            for (int i = 0; i < imgs.Length; i++)
            {
                content = content.Replace("[img" + i + "]", string.Format(format, imgs[i]));
            }
            return content;
        }

        public static string findImgPlaceholder(string content, int maxid = 3)
        {
            string result = "";
            for (int i = 0; i <= maxid; i++)
            {
                if (content.Contains("[img" + i + "]"))
                    result += "[img" + i + "]";
            }
            return result;
        }

        public static string removeImgPlaceholder(string content, int id, int maxid = 3)
        {
            content = content.Replace("[img" + id + "]", string.Empty);
            for (++id; id <= maxid; id++)
            {
                content = content.Replace("[img" + id + "]", "[img" + (id - 1) + "]");
            }
            return content;
        }

        public static string insertImgPlaceholder(string content, int maxid = 3)
        {

            for (int id = maxid - 1; id >= 0; id--)
            {
                content = content.Replace("[img" + id + "]", "[img" + (id + 1) + "]");
            }
            return content.Replace("[nimg0]", "[img0]");
        }
        public static string replaceNewImgPlaceholder(string content, int newid, int currentid,bool[] uploadpos, int maxid = 3)
        {
            for (; newid <= maxid; newid++)
            {
                if (uploadpos[newid])
                {
                    content = content.Replace("[nimg" + newid + "]", "[img" + currentid++ + "]");
                }
                else
                {
                    content = content.Replace("[nimg" + newid + "]", string.Empty);
                }
            }
            return content;
        }

        public static bool checkAdmin(bool includeWriter = false)
        {

            return (HttpContext.Current.User.IsInRole("Administrator") || HttpContext.Current.User.IsInRole("Moderator")) || ( includeWriter && HttpContext.Current.User.IsInRole("Writers"));
        }

        public static int getCaptchaId()
        {
            int? capthcaid = HttpContext.Current.Session["captchaid"] as int?;
            int prefix = 0;
            if (capthcaid.HasValue)
            {
                prefix = capthcaid.Value;
                HttpContext.Current.Session["captchaid"] = prefix == 5 ? 1 : (capthcaid.Value + 1);
            }
            return prefix;
        }

        public static bool checkCaptchaError(string Captcha, string Prefix, bool skipAdmin = false)
        {
            if (!skipAdmin && checkAdmin(true))
            {
                return false;
            }
            var Session = HttpContext.Current.Session;
            if (Session["Captcha" + Prefix] == null || Session["Captcha" + Prefix].ToString() != Captcha)
            {
                addCaptchaErrorCount(ref Session);
                return true;
            }
            else
            {
                Session["captchaerror"] = 0;
                return false;
            }
        }

        public static void addCaptchaErrorCount(ref System.Web.SessionState.HttpSessionState session)
        {
            if (session == null)
                return;
            int? errcount = session["captchaerror"] as int?;
            if (errcount.HasValue)
            {
                if (errcount.Value >= 10)
                    HttpContext.Current.Application["iptoban"] = HttpContext.Current.Request.UserHostAddress;
                else
                    session["captchaerror"] = errcount.Value + 1;
            } 
        }

        public static string splitIPtoTextArea(string ip)
        {
            string ips = null;
            if(!string.IsNullOrEmpty(ip))
                ips = string.Join("\n", ip.Split(new char[]{';',',',' ','，'}, StringSplitOptions.RemoveEmptyEntries));
            return ips;
        }

        public static bool checkBlogLinks(BlogLink[] blogLink)
        {
            if (blogLink == null || blogLink.Length == 0)
                return false;
            foreach (var link in blogLink)
            {
                if (string.IsNullOrEmpty(link.url))
                    return false;
                else if (link.url.ToUpperInvariant().Contains("JAVASCRIPT"))
                {
                    return false;
                }
            }
            return true;
        }

        public static string appendPassToContent(string content, BlogLink[] password)
        {
            if (password != null && password.Length > 0)
            {
                string pass = string.Empty;
                foreach (var item in password)
                {
                    if (!string.IsNullOrWhiteSpace(item.url))
                    {
                        pass += string.Format("{0}：<span class='label label-inverse'>{1}</span><br />", (string.IsNullOrWhiteSpace(item.name) ? "密码" : item.name), item.url);
                    }
                }
                content += string.Format("<p>{0}</p>", pass);
            }
            return content;
        }

        public static int getUnapproveCount()
        {
            using (var db = new BlogContext())
            {
                return db.Blogs.Count(b => b.isApproved == null && b.BlogID > 0);
            }
        }

        public static IQueryable<BlogWithRating> getSortedQuery(BlogContext db, IQueryable<Blog> blogquery, string sort)
        {
            var rankings = db.Ratings.GroupBy(r => r.BlogID).Join(blogquery, g => g.Key, b => b.BlogID, (g, b) => new BlogWithRating { blog = g.FirstOrDefault().blog, rating = g.Sum(r => r.value) });
            var query = blogquery.Except(rankings.Select(r => r.blog)).Select(q => new BlogWithRating { blog = q, rating = 0 }).Concat(rankings);
            switch (sort)
            {
                case "Date":
                    query = query.OrderBy(q => q.blog.BlogDate);
                    break;
                case "Visit_desc":
                    query = query.OrderByDescending(q => q.blog.BlogVisit);
                    break;
                case "Visit":
                    query = query.OrderBy(q => q.blog.BlogVisit);
                    break;
                case "Post":
                    query = query.OrderBy(q => q.blog.posts.Max(p => p.PostDate));
                    break;
                case "Post_desc":
                    query = query.OrderByDescending(q => q.blog.posts.Max(p => p.PostDate));
                    break;
                case "Rate":
                    query = query.OrderBy(q => q.rating);
                    break;
                case "Rate_desc":
                    query = query.OrderByDescending(q => q.rating);
                    break;
                default:
                    query = query.OrderByDescending(q => q.blog.BlogDate);
                    break;
            }
            return query;
        }
        public static IQueryable<SearchDisplay> getSortedQuery(IQueryable<SearchDisplay> query, string sort)
        {
            var sorted = query.OrderByDescending(q => q.itemType);
            switch (sort)
            {
                case "Date":
                    query = sorted.ThenBy(q => q.Date);
                    break;
                case "Visit_desc":
                    query = sorted.ThenByDescending(q => q.Visit);
                    break;
                case "Visit":
                    query = sorted.ThenBy(q => q.Visit);
                    break;
                case "Post":
                    query = sorted.ThenBy(q => q.LastPost);
                    break;
                case "Post_desc":
                    query = sorted.ThenByDescending(q => q.LastPost);
                    break;
                default:
                    query = sorted.ThenByDescending(q => q.Date);
                    break;
            }
            return query;
        }
        public static IQueryable<Topic> getSortedQuery(IQueryable<Topic> query, string sort)
        {
            switch (sort)
            {
                case "Date":
                    query = query.OrderBy(q => q.CreateDate);
                    break;
                case "Update_desc":
                    query = query.OrderByDescending(q => q.UpdateDate);
                    break;
                case "Update":
                    query = query.OrderBy(q => q.UpdateDate);
                    break;
                case "Visit_desc":
                    query = query.OrderByDescending(q => q.TopicVisit);
                    break;
                case "Visit":
                    query = query.OrderBy(q => q.TopicVisit);
                    break;
                case "Post":
                    query = query.OrderBy(q => q.Posts.Max(p => p.PostDate));
                    break;
                case "Post_desc":
                    query = query.OrderByDescending(q => q.Posts.Max(p => p.PostDate));
                    break;
                default:
                    query = query.OrderByDescending(q => q.CreateDate);
                    break;
            }
            return query;
        }
        public static string getSortName(string currentsort, out string nextsort, out bool isDesc)
        {
            string name;
            switch (currentsort)
            {
                case "Date":
                    name = "发布日期";
                    nextsort = "Date_desc";
                    isDesc = false;
                    break;
                case "Update_desc":
                    name = "更新日期";
                    nextsort = "Update";
                    isDesc = true;
                    break;
                case "Update":
                    name = "更新日期";
                    nextsort = "Update_desc";
                    isDesc = false;
                    break;
                case "Visit_desc":
                    name = "浏览数";
                    nextsort = "Visit";
                    isDesc = true;
                    break;
                case "Visit":
                    name = "浏览数";
                    nextsort = "Visit_desc";
                    isDesc = false;
                    break;
                case "Post":
                    name = "最后评论";
                    nextsort = "Post_desc";
                    isDesc = false;
                    break;
                case "Post_desc":
                    name = "最后评论";
                    nextsort = "Post";
                    isDesc = true;
                    break;
                case "Rate":
                    name = "评分";
                    nextsort = "Rate_desc";
                    isDesc = false;
                    break;
                case "Rate_desc":
                    name = "评分";
                    nextsort = "Rate";
                    isDesc = true;
                    break;
                default:
                    name = "发布日期";
                    nextsort = "Date";
                    isDesc = true;
                    break;
            }
            return name;
        }

        public static List<int> getBlogIDinTopic(int TopicId)
        {
            using (var db = new BlogContext())
            {
                return db.BlogsInTopics.Where(t => t.TopicID == TopicId).OrderBy(t => t.BlogOrder).Select(t => t.BlogID).ToList();
            }
        }
        public static string GetUnreadMsg(string name)
        {
            int count = 0;
            using (var db = new UsersContext())
            {
                count = db.Messages.Count(m => m.Recipient == name && !m.IsRead && !m.IsRecipientDelete);
            }
            if (count > 0)
            {
                return count.ToString();
            }
            return null;
        }
        public static void sendPWEmailForUser(UserProfile user, string reseturl)
        {
            MailMessage mail = new MailMessage(new MailAddress("admin@gmgard.us", "gmgard.us"), new MailAddress(user.Email, user.UserName));
            //mail.Sender = mail.From;
            mail.Subject = "重设密码请求";
            mail.Body = string.Format(
                "{0}，您好！<br>您在绅士之庭（<a href='http://gmgard.us'>gmgard.us</a>）申请了重设密码。请点击下面的链接继续您的重设密码操作。<br><br><a href='{1}'>{1}</a><br><br>" +
                "如果您没有进行重设密码申请，那么可能是其他人输错了邮箱地址。敬请忽略此邮件，您的密码将不会改变。<br>" +
                "请不要公开您的账号密码，并将密码记录在安全的位置。如有任何疑问，请直接回复此邮件以联系管理员。<br>绅士之庭随时欢迎您的光临！<br>",
                user.UserName, reseturl + WebMatrix.WebData.WebSecurity.GeneratePasswordResetToken(user.UserName)
                );
            mail.IsBodyHtml = true;
            new SmtpClient().Send(mail);
        }

        #region 标签处理
        public static int CheckBlogTag(string tagstring, int maxcount = 10)
        {
            if(tagstring == null || tagstring.Length == 0)
                return 0;
            else if (tagstring.Length > 100)
                return -1;
            string[] tags = SplitTags(tagstring);
            if (tags.Length > maxcount)
                return 1;
            foreach (var tag in tags)
            {
                if (tag.Length > 20)
                    return -1;
            }
            return 0;
        }

        public static string[] SplitTags(string tagstring)
        {
            if (tagstring == null) return null;
            return tagstring.Split(new char[] { '，', '。', ',', ';', '；', '、', ' ' }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
        }

        public static string[] GetTagNamesInBlog(int blogid)
        {
            string[] tags;
            using (var db = new BlogContext())
            {
                tags = db.TagsInBlogs.Where(t => t.BlogID == blogid).Select(t => t.tag.TagName).ToArray();
            }
            return tags;
        }

        public static Tag[] GetTagsInBlog(int blogid)
        {
            using (var db = new BlogContext())
            {
                return db.TagsInBlogs.Where(t => t.BlogID == blogid).Select(t => t.tag).ToArray();
            }
        }
        public static int AddTagForBlog(BlogContext db, int blogid, string tag)
        {
            Tag t = db.Tags.SingleOrDefault(tt => tt.TagName == tag);
            if (t == null)
            {
                t = new Tag { TagName = tag };
            }
            TagsInBlog tib = new TagsInBlog { BlogID = blogid };
            tib.tag = t;
            db.TagsInBlogs.Add(tib);
            db.SaveChanges();
            return tib.TagID;
        }

        public static void AddTagsForBlog(int blogid, string[] tags)
        {
            using (var db = new BlogContext())
            {
                foreach (string tag in tags)
                {
                    Tag t = db.Tags.SingleOrDefault(tt => tt.TagName == tag);
                    if (t == null)
                    {
                        t = new Tag { TagName = tag };
                    }
                    TagsInBlog tib = new TagsInBlog { BlogID = blogid };
                    tib.tag = t;
                    db.TagsInBlogs.Add(tib);
                }
                db.SaveChanges();
            }
        }

        public static void SetTagsForBlog(int blogid, string[] tags)
        {
            using (var db = new BlogContext())
            {
                bool hasdelete = false;
                var tagcurrent = db.TagsInBlogs.Where(i => i.BlogID == blogid).Select(i => new { name = i.tag.TagName, id = i.tag.TagID});
                var tagtodel = tagcurrent.Where(a => !tags.Contains(a.name));
                foreach (var tag in tagtodel)
                {
                    TagsInBlog tib = db.TagsInBlogs.Find(blogid, tag.id);
                    db.TagsInBlogs.Remove(tib);
                    hasdelete = true;
                }
                var tagtoadd = tags.Except(tagcurrent.Select(a => a.name));
                foreach (var tag in tagtoadd)
                {
                    Tag t = db.Tags.SingleOrDefault(tt => tt.TagName == tag);
                    if (t == null)
                    {
                        t = new Tag { TagName = tag };
                    }
                    TagsInBlog tib = new TagsInBlog();
                    tib.tag = t;
                    tib.BlogID = blogid;
                    db.TagsInBlogs.Add(tib);
                }
                db.SaveChanges();
                if(hasdelete)
                    db.Database.ExecuteSqlCommand("Delete From Tags Where TagID not in ((Select TagID from TagsInBlogs) union (Select TagID from Topics))");
            }
        }

        public static void DelTagsInBlog(int blogid)
        {
            using (var db = new BlogContext())
            {
                var query = db.TagsInBlogs.Where(i => i.BlogID == blogid);
                bool removed = false;
                foreach (TagsInBlog tib in query)
                {
                    db.TagsInBlogs.Remove(tib);
                    removed = true;
                }
                if (removed)
                {
                    db.SaveChanges();
                    db.Database.ExecuteSqlCommand("Delete From Tags Where TagID not in ((Select TagID from TagsInBlogs) union (Select TagID from Topics))");
                }
            }
        }
        #endregion

        #region 图片处理
        public static bool AddPic(string name,string type,Stream data)
            {
                Pictures p = new Pictures();
                p.PicName = name;
                p.PicType = type;
                Bitmap bmp = new Bitmap(data);
                ImageConverter converter = new ImageConverter();
                p.Data = (byte[])converter.ConvertTo(bmp, typeof(byte[]));
                using ( var _db = new UsersContext()){
                    _db.Avatars.Add(p);
                    _db.SaveChanges();
                }
                return true;
            }
            public static bool AddPic(string name, string type, byte[] data)
            {
                Pictures p = new Pictures();
                p.PicName = name;
                p.PicType = type;
                p.Data = data;
                //p.Thumb = (byte[])converter.ConvertTo(thumb, typeof(byte[]));
                using ( var _db = new UsersContext()){
                    _db.Avatars.Add(p);
                    _db.SaveChanges();
                }
                return true;
            }
            public static bool HasPic(string name)
            {
                if (name == null)
                    return false;
                using (var _db = new UsersContext()){
                    Pictures p = _db.Avatars.SingleOrDefault(pp => pp.PicName == name);
                
                if (p == null)
                    return false;
                else
                    return true;
                }
            }
            public static bool UpdateOrAddPic(string name, string type, byte[] data)
            {
                using (var _db = new UsersContext()){
                    Pictures p = _db.Avatars.SingleOrDefault(pp => pp.PicName == name);
                    if (p == null)
                    {
                        AddPic(name, type, data);
                        return true;
                    }
                    p.PicType = type;
                    p.Data = data;
                    _db.SaveChanges();
                }
                return true;
            }

            public static Image GetThumb(Image bmp, int max)
            {
                Size s = bmp.Size;
                if (s.Height <= max && s.Width <=max)
                    return bmp;
                if (s.Height > s.Width)
                {
                    double ratio = (double)max / s.Height;
                    s.Height = max;
                    s.Width = (int)(s.Width * ratio);
                    if (s.Width <= 0)
                        s.Width = 1;
                }
                else
                {
                    double ratio = (double)max / s.Width;
                    s.Width = max;
                    s.Height = (int)(s.Height * ratio);
                    if (s.Height <= 0)
                        s.Height = 1;
                }
                return new Bitmap(bmp, s);
            }
            //public static byte[] CreateAvatar(Stream buff, int maxHeight)
            //{
            //    Bitmap bmp = new Bitmap(buff);
            //    //Bitmap thumb = GetThumb(bmp, maxHeight);
            //    ImageConverter converter = new ImageConverter();
            //    return (byte[])converter.ConvertTo(bmp, typeof(byte[]));
            //}
            
            public static byte[] Crop(byte[] Img, int Width, int Height, int X, int Y)
            {
                using (var stream = new MemoryStream(Img))
                {
                    return Crop(stream, Width, Height, X, Y);
                }
            }

            public static byte[] Crop(MemoryStream stream, int Width, int Height, int X, int Y)
            {
                using (Image OriginalImage = Image.FromStream(stream))
                {
                    if (X + Width >= OriginalImage.Size.Width || Y + Height >= OriginalImage.Size.Height)
                    {
                        ImageConverter converter = new ImageConverter();
                        return (byte[])converter.ConvertTo(OriginalImage, typeof(byte[]));
                    }
                    using (Bitmap bmp = new Bitmap(Width, Height))
                    {
                        bmp.SetResolution(OriginalImage.HorizontalResolution, OriginalImage.VerticalResolution);
                        using (Graphics Graphic = Graphics.FromImage(bmp))
                        {
                            Graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                            Graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            Graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                            var img = OriginalImage;
                            if (OriginalImage.Size.Height > 500 || OriginalImage.Size.Width > 500)
                                img = GetThumb(OriginalImage, 500);
                            Graphic.DrawImage(img, new Rectangle(0, 0, Width, Height), X, Y, Width, Height, GraphicsUnit.Pixel);
                            stream.Dispose();
                            var mstream = new MemoryStream();
                            bmp.Save(mstream, OriginalImage.RawFormat);
                            return mstream.GetBuffer();
                        }
                    }
                }
            }

            public static string saveImage(HttpPostedFileBase file)
            {
                string path = HttpContext.Current.Server.MapPath("~/Images/");
                string imgname;
                imgname = file.FileName;
                int i = imgname.IndexOf('.');
                imgname = imgname.Substring(0,3) + (new Random().Next(100)) + DateTime.Now.ToString("ddHHmmssffff") + ".jpg";
                using (var img = new Bitmap(file.InputStream))
                using (var bmp = GetThumb(img, 1000))
                    bmp.Save(path + "upload/" + imgname, System.Drawing.Imaging.ImageFormat.Jpeg);
                return imgname;
            }
            public static List<string> saveImages(IEnumerable<HttpPostedFileBase> BlogImage)
            {
                string imgname;
                List<string> imglist = new List<string>();
                foreach (var file in BlogImage)
                {
                    if (file != null)
                    {
                        try
                        {
                            imgname = saveImage(file);
                            imglist.Add(imgname);
                        }
                        catch
                        {
                            removeFiles(imglist);
                            throw ;
                        }
                    }
                }
                return imglist;
            }

            public static void savethumb(string imgname)
            {
                string path = HttpContext.Current.Server.MapPath("~/Images/");
                int s = imgname.IndexOf(';');
                if (s >= 0)
                {
                    imgname = imgname.Substring(0, s);
                }
                using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(path + "upload/" + imgname))
                using (var thumb = GetThumb(bmp, 100))
                {
                    thumb.Save(path + "thumbs/" + imgname);
                }
            }
            public static void removethumb(string imgname)
            {
                string path = HttpContext.Current.Server.MapPath("~/Images/");
                File.Delete(path + "thumbs/" + imgname);
            }

            public static List<string> checkDeletedImgs(string original, string current)
            {
                string[] imglisto = original.Split(';');
                string[] imglistc = current.Split(';');
                List<string> resultlist = new List<string>();
                foreach (string name in imglisto)
                //foreach name in orignial imglist, if current imglist does not contain the name,delete it
                {
                    if (!imglistc.Contains(name))
                        resultlist.Add(name);
                }
                return resultlist;
            }
            public static bool checkFieldValid(string original, string current)
            {
                string[] imglisto = original.Split(';');
                string[] imglistc = current.Split(';');
                foreach (string name in imglistc)
                //foreach name in current imglist, if orignal imglist does not contain the name,it is not valid
                {
                    if (!imglisto.Contains(name))
                        return false;
                }
                return true;
            }
            public static void removeFiles(IEnumerable<string> names, bool hasthumb = false)
            {
                if (names == null)
                    return;
                String path = HttpContext.Current.Server.MapPath("~/Images/");
                foreach (string imgname in names)
                {
                    File.Delete(path + "upload/" + imgname);
                }
                if (hasthumb)
                    File.Delete(path + "thumbs/" + names.First());
            }
        #endregion

    }
}