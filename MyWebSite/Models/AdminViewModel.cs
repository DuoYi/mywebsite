﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MyMVCWeb.Models
{
    public class AppSettingsModel
    {
        //public Dictionary<string, string> settings { get; set; }
        public string blockip { get; set; }
        public string updateinterval { get; set; }
        public string expaddonpass { get; set; }
        public string expaddondays { get; set; }
        public string listpagesize { get; set; }
        public string homepagesize { get; set; }
        public string userpagesize { get; set; }
        public string auditpagesize { get; set; }
        public string replypagesize { get; set; }
        public string msgpagesize { get; set; }
        public string rankingsize { get; set; }
    }
    public class DataSettingsModel
    {
        public string featuredblogids { get; set; }
    }
    public class AdminViewModel
    {
        public List<UserProfile> Admins {get; set;}
        public List<UserProfile> Writers { get; set; }
        public List<Category> allCategory { get; set; }
        public int totalauditcount { get; set; }
        public int auditcount { get; set; }
        public int totalusercount { get; set; }
        public int todaynewitem { get; set; }
        public int yesterdaynewitem { get; set; }
        public int bannedusercount { get; set; }
        public AppSettingsModel appsettings { get; set; }
        public DataSettingsModel datasettings { get; set; }

        public static List<UserProfile> getAdmins()
        {
            List<UserProfile> result = new List<UserProfile>();
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MyMvcWebUser"].ConnectionString;
            string query = "Select p.userid, username, email, LastLoginDate from [userprofile] as p inner join " +
                            "(select RoleName, UserId from [webpages_Roles] as r inner join [webpages_UsersInRoles] " +
                            "as ur on r.RoleId = ur.RoleId where RoleName = 'Administrator' or RoleName = 'Moderator') as u on  p.UserId = u.UserId";
            using(SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataReader queryCommandReader = cmd.ExecuteReader();
                while(queryCommandReader.Read())
                {
                    IDataRecord data = queryCommandReader;
                    UserProfile p = new UserProfile();
                    p.UserId = (int)data["userid"];
                    p.UserName = data["username"] as string;
                    p.Email = data["email"] as string;
                    p.LastLoginDate = (DateTime)data["LastLoginDate"];
                    result.Add(p);
                }
                //conn.Close();
            }
            return result;
        }
        public static List<UserProfile> getWriters()
        {
            List<UserProfile> result = new List<UserProfile>();
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MyMvcWebUser"].ConnectionString;
            string query = "Select p.userid, username, email, LastLoginDate from [userprofile] as p inner join " +
                            "(select RoleName, UserId from [webpages_Roles] as r inner join [webpages_UsersInRoles] " +
                            "as ur on r.RoleId = ur.RoleId where RoleName = 'Writers') as u on  p.UserId = u.UserId";
            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataReader queryCommandReader = cmd.ExecuteReader();
                while (queryCommandReader.Read())
                {
                    IDataRecord data = queryCommandReader;
                    UserProfile p = new UserProfile();
                    p.UserId = (int)data["userid"];
                    p.UserName = data["username"] as string;
                    p.Email = data["email"] as string;
                    p.LastLoginDate = (DateTime)data["LastLoginDate"];
                    result.Add(p);
                }
                //conn.Close();
            }
            return result;
        }
    }

}