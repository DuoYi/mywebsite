﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyMVCWeb.Models
{
    public class BlogDBinit : CreateDatabaseIfNotExists<BlogContext>
    {
        protected override void Seed(BlogContext context)
        {
            
            GetCategories().ForEach(c => context.Categories.Add(c));
            GetBlogs().ForEach(p => context.Blogs.Add(p));
            //context.Database.ExecuteSqlCommand("set Identity_insert Blogs on");
            context.SaveChanges();
            context.Database.ExecuteSqlCommand("set Identity_insert Blogs on Insert into dbo.Blogs (blogid, BlogTitle, Content, ImagePath, isLocalImg, BlogDate,CategoryID, Author, isApproved, BlogVisit)" +
                                            "values (0, 'V0.01', '版本历史', null, 'false', GETDATE(), 1, 'admin', 'false', 0)" +
                                            "Insert into dbo.Blogs (blogid, BlogTitle, Content, ImagePath, isLocalImg, BlogDate,CategoryID, Author, isApproved, BlogVisit)" +
                                            "values (-1, '举报消息', '举报消息', null, 'false', GETDATE(), 1, 'admin', 'false', 0)");
            context.Database.ExecuteSqlCommand("set Identity_insert Blogs off");
        }
        private static List<Category> GetCategories()
        {
            var categories = new List<Category> {
            new Category
            {
                CategoryID = 1,
                CategoryName = "综合"
            },
            new Category
            {
                CategoryID = 2,
                CategoryName = "随笔"
            },
            new Category
            {
                CategoryID = 3,
                CategoryName = "游记"
            },
            new Category
            {
                CategoryID = 4,
                CategoryName = "游戏"
            },
            new Category
            {
                CategoryID = 5,
                CategoryName = "音乐"
            }
        };

            return categories;
        }
        private static List<Blog> GetBlogs()
        {
            var blogs = new List<Blog> {
            new Blog
            {
                BlogID = 1,
                BlogTitle = "Hello World",
                Content = "This is the first blog in this web! Cheers!!!", 
                ImagePath="hello.png",
                BlogDate=DateTime.Now,
                CategoryID = 1,
                Author = "admin",
                isApproved = true,
                isLocalImg = true,
                BlogVisit = 0,
                posts = new List<Post>{
                        new Post{
                            Author = "admin",
                            Content = "<p>First Reply</p>",
                            PostDate = DateTime.Now,
                            PostId = 1,
                        }
                    }
            }
        };
            return blogs;
        }
    }
}