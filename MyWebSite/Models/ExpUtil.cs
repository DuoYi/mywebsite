﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;

namespace MyMVCWeb.Models
{
    public class ExpUtil
    {
        public static void calculateUserLevel(string username, out int level, out string title)
        {
            level = 0;
            title = string.Empty;
            using (var db = new UsersContext())
            {
                var user = db.UserProfiles.Single(u => u.UserName == username);
                var exp = db.ExpTable.SingleOrDefault(e => e.ExperienceStart <= user.Experience && e.ExperienceEnd >= user.Experience);
                if (exp != null)
                {
                    level = exp.Level;
                    title = exp.Title;
                }
            }
        }

        public static int calculateUserLevel(int userid)
        {
            int lvl = 0;
            using (var db = new UsersContext())
            {
                var user = db.UserProfiles.Find(userid);
                var exp = db.ExpTable.SingleOrDefault(e => e.ExperienceStart <= user.Experience && e.ExperienceEnd >= user.Experience);
                if (exp != null)
                {
                    lvl = exp.Level;
                }
            }
            return lvl;
        }

        public static string getLevelTitle(int level)
        {
            using (var db = new UsersContext())
            {
                var lvl = db.ExpTable.Find(level);
                if (lvl != null)
                    return lvl.Title;
            }
            return null;
        }

        public static int getUserLvl(string username)
        {
            using (var db = new UsersContext())
            {
                var user= db.UserProfiles.Single(u => u.UserName == username);
                return user.Level;
            }
        }

        public static void getUserExp(string username, out int expCurrent, out int expNext)
        {
            expCurrent = 0;
            expNext = 0;
            using (var db = new UsersContext())
            {
                var user = db.UserProfiles.Single(u => u.UserName == username);
                var exp = db.ExpTable.Find(user.Level);
                if (exp != null && exp.ExperienceEnd > 0)
                {
                    expCurrent = user.Experience - exp.ExperienceStart;
                    expNext = exp.ExperienceEnd - exp.ExperienceStart + 1;
                }
            }
        }

        public static void addExp(string username, int expCount)
        {
            using (var db = new UsersContext())
            {
                var user = db.UserProfiles.Single(u => u.UserName == username);
                user.Experience += expCount;
                var exp = db.ExpTable.SingleOrDefault(e => e.ExperienceEnd >= user.Experience && e.ExperienceStart <= user.Experience);
                if (exp != null && exp.Level >= user.Level)
                {
                    user.Level = exp.Level;
                }
                db.SaveChanges();
            }
        }

        public static int hasSigned()
        {
            using (var userdb = new UsersContext())
            {
                var profile = userdb.UserProfiles.Find(WebSecurity.CurrentUserId);
                TimeSpan diffday = DateTime.Now - profile.LastSignDate;
                if (diffday.Days >= 1)
                {
                    return 0;
                }
                else
                {
                    return profile.ConsecutiveSign;
                }
            }
        }

        internal static void addExpOnPass(string p1, object p2)
        {
            if (p2 is int)
            {
                addExp(p1, (int)p2);
            }
            else
            {
                addExp(p1, 10);
            }
        }
    }
}