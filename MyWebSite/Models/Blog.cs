﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyMVCWeb.Models
{
    public class BlogContext : DbContext
    {
        public BlogContext()
            : base("MyMvcWebData")
        {
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagsInBlog> TagsInBlogs { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Reply> Replies { get; set; }
        public DbSet<BlogsInTopic> BlogsInTopics { get; set; }
        public DbSet<Advertisment> Advertisments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Blog>()
                        .HasRequired(e => e.category)
                        .WithMany(c => c.Blogs)
                        .HasForeignKey(e => e.CategoryID)
                        .WillCascadeOnDelete(false);
        }
    }

    public class Category
    {
        [ScaffoldColumn(false)]
        public int CategoryID { get; set; }
        
        [Required, StringLength(100), Display(Name = "Name")]
        public string CategoryName { get; set; }

        [Display(Name = "Category Description")]
        [MaxLength(256)]
        public string Description { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }
    }
    public class Blog
    {
        [ScaffoldColumn(false),DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int BlogID { get; set; }

        [Required(ErrorMessage = "请输入标题"), StringLength(80, ErrorMessage = "标题不得超过80个字符"), Display(Name = "标题")]
        public string BlogTitle { get; set; }

        [Required(ErrorMessage = "请输入内容"), StringLength(int.MaxValue), Display(Name = "内容"), DataType(DataType.MultilineText)]
        public string Content { get; set; }
        [MaxLength(1024)]
        public string ImagePath { get; set; }

        public bool isLocalImg { get; set; }

        [Display(Name = "日期")]
        public DateTime BlogDate { get; set; }

        [Required]
        public int CategoryID { get; set; }
        [ForeignKey("CategoryID")]
        public Category category { get; set; }
        [Required]
        [MaxLength(30)]
        public string Author { get; set; }

        public bool? isApproved { get; set; }

        [DefaultValue(0)]
        public long BlogVisit { get; set; }

        public string Links { get; set; }

        public virtual List<Post> posts { get; set; }
    }

    public class Post
    {
        public int PostId { get; set; }
        public DateTime PostDate { get; set; }
        [MaxLength(30)]
        public string Author { get; set; }
        [Required(ErrorMessage = "请输入内容"), StringLength(int.MaxValue), DataType(DataType.MultilineText)]
        public string Content { get; set; }
        [ForeignKey("BlogId")]
        public virtual Blog blog { get; set; }
        public int? BlogId { get; set; }
        [ForeignKey("TopicId")]
        public virtual Topic topic { get; set; }
        public int? TopicId { get; set; }

        public virtual List<Reply> repies { get; set; }
    }
    public class Reply
    {
        public int ReplyId { get; set; }
        public DateTime ReplyDate { get; set; }
        [MaxLength(30)]
        public string Author { get; set; }
        [Required(ErrorMessage = "请输入内容"), StringLength(int.MaxValue), DataType(DataType.MultilineText)]
        public string Content { get; set; }
        [ForeignKey("PostId")]
        public virtual Post post { get; set; }
        public int PostId { get; set; }
    }

    public class Rating
    {
        [Key]
        public Guid RatingID { get; set; }
        public int BlogID { get; set; }
        [ForeignKey("BlogID")]
        public Blog blog { get; set; }

        [Required]
        public int value { get; set; }
        [MaxLength(50)]
        public string ip { get; set; }
        public DateTime ratetime { get; set; }
    }

    public class Tag
    {
        [Key]
        public int TagID { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(30)]
        public string TagName { get; set; }
        [DefaultValue(0)]
        public long TagVisit { get; set; }
    }

    public class TagsInBlog
    {
        [Key, Column(Order = 1)]
        public int BlogID { get; set; }
        [ForeignKey("BlogID")]
        public virtual Blog blog { get; set; }
        [Key, Column(Order = 2)]
        public int TagID { get; set; }
        [ForeignKey("TagID")]
        public virtual Tag tag { get; set; }
    }

    public class Topic
    {
        [Key]
        public int TopicID { get; set; }
        [Required(ErrorMessage = "请输入标题"), StringLength(80, ErrorMessage = "标题不得超过80个字符")]
        public string TopicTitle { get; set; }

        [Required(ErrorMessage = "请输入内容"), DataType(DataType.MultilineText)]
        public string Content { get; set; }
        [MaxLength(512)]
        public string ImagePath { get; set; }

        public bool isLocalImg { get; set; }

        [Display(Name = "创建日期")]
        public DateTime CreateDate { get; set; }
        [Display(Name = "更新日期")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [MaxLength(30)]
        public string Author { get; set; }

        [DefaultValue(0)]
        public long TopicVisit { get; set; }

        [Required]
        public int CategoryID { get; set; }

        [Required]
        public int TagID { get; set; }
        [ForeignKey("TagID")]
        public virtual Tag tag { get; set; }

        public virtual List<Post> Posts { get; set; }   
    }

    public class BlogsInTopic
    {
        [Key, Column(Order = 1)]
        public int TopicID { get; set; }
        [ForeignKey("TopicID")]
        public virtual Topic topic { get; set; }
        [Key, Column(Order = 2)]
        public int BlogID { get; set; }
        [ForeignKey("BlogID")]
        public virtual Blog blog { get; set; }
        
        public int BlogOrder { get; set; }
    }

    public class Advertisment
    {
        [Key]
        public int AdID { get; set; }
        [MaxLength(1000),DataType(DataType.Url),Required]
        public string AdUrl { get; set; }
        [MaxLength(1000),DataType(DataType.ImageUrl)]
        public string ImgUrl { get; set; }
        [MaxLength(100), Required]
        public string AdTitle { get; set; }

        public long ExposeCount { get; set; }
        public int ClickCount { get; set; }
    }
}