﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MyMVCWeb.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("MyMvcWebUser")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<UserOption> UserOptions { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Pictures> Avatars { get; set; }
        public DbSet<ExperienceTable> ExpTable { get; set; }
    }

    public class Message
    {
        [Key, ScaffoldColumn(false)]
        public int MsgId { get; set; }
        [Required]
        [MaxLength(30)]
        public string Sender { get; set; }
        [Required(ErrorMessage="请输入收件人")]
        [RegularExpression("^[a-zA-Z0-9_\u2E80-\u9FFF]{1,20}$", ErrorMessage = "用户名包含无效字符")]
        [Remote("CheckUsername", "Message", ErrorMessage = "查无此人", HttpMethod = "Post")]
        [MaxLength(30)]
        public string Recipient { get; set; }
        public DateTime MsgDate { get; set; }
        [Required(ErrorMessage="请输入正文"), DataType(DataType.MultilineText)]
        public string MsgContent { get; set; }
        [MaxLength(80,ErrorMessage="标题不得超过80字符")]
        public string MsgTitle { get; set; }
        [DefaultValue(false)]
        public bool IsRead { get; set; }
        [DefaultValue(false)]
        public bool IsSenderDelete { get; set; }
        [DefaultValue(false)]
        public bool IsRecipientDelete { get; set; }
    }

    public class Pictures
    {
        [ScaffoldColumn(false), Key]
        public int PicID { get; set; }
        [Required][MaxLength(50)]
        public string PicType { get; set; }
        [Required][MaxLength(256)]
        public string PicName { get; set; }
        [DataType(DataType.Upload), Required]
        public byte[] Data { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [Required][MaxLength(20)]
        public string UserName { get; set; }
        [MaxLength(200,ErrorMessage="签名不得超过200字符")]
        public string UserComment { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)][MaxLength(50)]
        public string Email { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime LastLoginDate { get; set; }
        [DataType(DataType.Text)]
        [MaxLength(80)]
        public string LastLoginIP { get; set; }
        [Required]
        [DefaultValue(0)]
        public int Points { get; set; }
        public int Experience { get; set; }
        public int Level { get; set; }
        [DataType(DataType.Date)]
        public DateTime LastSignDate { get; set; }
        public int ConsecutiveSign { get; set; }
        public virtual UserOption option { get; set; }
    }

    public class UserOption
    {
        public virtual UserProfile user { get; set; }
        [Key, ForeignKey("user")]
        public int UserId { get; set; }
        public bool sendNoticeForNewReply{get;set;}
        public bool sendNoticeForNewPostReply { get; set; }
    }

    public class CommentChangeModel
    {
        [MaxLength(200, ErrorMessage = "签名不得超过200字符")]
        public string comment { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "当前密码")]
        [AllowHtml]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} 必须至少包含 {2} 个字符。", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "新密码")]
        [AllowHtml]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "确认新密码")]
        [AllowHtml]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "新密码和确认密码不匹配。")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required(ErrorMessage="请输入用户名")]
        [Display(Name = "用户名")]
        [RegularExpression("^[a-zA-Z0-9_\u2E80-\u9FFF]{1,20}$", ErrorMessage = "用户名包含无效字符")]
        [MaxLength(20)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "请输入密码")]
        [DataType(DataType.Password)]
        [Display(Name = "密码")]
        [AllowHtml]
        public string Password { get; set; }

        [Display(Name = "记住我?")]
        public bool RememberMe { get; set; }

        [Required(ErrorMessage = "请输入验证码")]
        [Display(Name = "等于多少?")]
        public string Captcha { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessage = "请输入用户名")]
        [Display(Name = "用户名")]
        [Remote("CheckUsername", "Account", ErrorMessage = "该用户名已注册", HttpMethod = "Post")]
        [RegularExpression("^[a-zA-Z0-9_\u2E80-\u9FFF]{1,20}$", ErrorMessage = "用户名包含无效字符")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "请输入密码")]
        [AllowHtml]
        [StringLength(100, ErrorMessage = "{0} 必须至少包含 {2} 个字符。", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "密码")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "确认密码")]
        [AllowHtml]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password", ErrorMessage = "密码和确认密码不匹配。")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "请输入邮箱")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "电子邮箱")]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$",ErrorMessage="无效的电子邮箱")]
        [Remote("CheckEmail", "Account", HttpMethod="Post")]
        public string Email { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "头像（可选）")]
        [ValidateFile(1024*1024,ErrorMessage="头像必须是图片，且不得超过1MB")]
        public HttpPostedFileBase avatar { get; set; }

        [Required(ErrorMessage = "请输入验证码")]
        [Display(Name = "等于多少?")]
        public string Captcha { get; set; }
        
        [DefaultValue(0)]
        public int X { get; set; }
        [DefaultValue(0)]
        public int Y { get; set; }
        [DefaultValue(150)]
        public int W { get; set; }
        [DefaultValue(150)]
        public int H { get; set; }
    }
    
    public class PasswordResetModel
    {
        [Required]
        public string Code { get; set; }

        [Required(ErrorMessage = "请输入密码")]
        [AllowHtml]
        [StringLength(100, ErrorMessage = "{0} 必须至少包含 {2} 个字符。", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "密码")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "确认密码")]
        [AllowHtml]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password", ErrorMessage = "密码和确认密码不匹配。")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }

    public class ExperienceTable
    {
        public int ExperienceStart { get; set; }
        public int ExperienceEnd { get; set; }
        [Key,DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Level { get; set; }
        [MaxLength(10)]
        public string Title { get; set; }
    }

    public class ValidateFileAttribute : RequiredAttribute
    {
        private int filesize;
        public ValidateFileAttribute(int filesize){
            this.filesize = filesize;
        }
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if(file == null)
                return true;
            else if (file.ContentLength > filesize || !file.ContentType.Contains("image"))
            {
                return false;
            }

            return true;
        }
    }
}
