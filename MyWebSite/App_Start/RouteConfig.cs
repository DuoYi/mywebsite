﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MyMVCWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Avatar", "Avatar/{name}",
                defaults: new { controller = "Avatar", action = "Show", name = UrlParameter.Optional }
                );
            //routes.MapRoute("BlogList", "BlogList/{id}",
            //    defaults: new { controller = "BlogList", action = "List", id = UrlParameter.Optional }
            //    );
            routes.MapRoute(
                name: "UserInfo",
                url: "User/{name}",
                defaults: new { controller = "Home", action = "UserInfo", name = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Upload",
                url: "Upload/{*img}",
                defaults: new { controller = "Upload", action = "Index", img = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}