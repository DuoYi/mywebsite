﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentScheduler;
using MyMVCWeb.Models;
using System.IO;

namespace MyMVCWeb.App_Start
{

    public class SchedulerConfig : Registry
    {
        public SchedulerConfig(int interval, int ranksize)
        {
            if (interval <= 0)
                interval = 10;
            if (ranksize <= 0)
                ranksize = 5;
            Action timertask = new Action( () =>{
                string result;
                using (var db = new BlogContext())
                {
                    var rankings = db.Ratings.GroupBy(r => r.BlogID).Select(g => new { blogId = g.Key, rating = g.Sum(r => r.value) }).OrderByDescending(a => a.rating).Take(ranksize)
                        .Join(db.Blogs.Where(b => b.isApproved == true), a => a.blogId, b => b.BlogID, (a, b) =>
                            new Ranking { blogId = b.BlogID, blogTitle = b.BlogTitle, blogThumb = b.ImagePath, isLocalImg = b.isLocalImg, rating = a.rating });
                    result = Newtonsoft.Json.JsonConvert.SerializeObject(rankings);
                }
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~/Scripts/ranking.js");
                File.WriteAllText(path, result);
            });
            Schedule(timertask).ToRunNow().AndEvery(10).Minutes();
        }

    }
}