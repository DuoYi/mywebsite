﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.toolbar = [
	{ name: 'undo', groups: ['undo'], items: ['Undo', 'Redo'] },
	{ name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
    { name: 'pics', items: ['Image']},
	{ name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar', 'Custom_Smiley'] },
	{ name: 'tools', items: ['Maximize'] },
	{ name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source'] },
	{ name: 'others', items: ['-'] },
	{ name: 'colors', items: ['TextColor', 'BGColor'] },
	'/',
	{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat'] },
	{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] },
	{ name: 'styles', items: ['Styles', 'Format','FontSize'] },
	{ name: 'about', items: ['About'] }
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;h4;h5;pre';

	config.image_previewText = "这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字这里是预览文字";

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	config.removePlugins = 'elementspath';

	config.extraAllowedContent = 'a[rel]; span(*)';
	config.extraPlugins = 'custom_smiley,colorbutton,font';
	config.smiley_path = '/Images/smileys/';
	config.smiley_images = [
                        '蛤蛤嘲讽.gif',  '蛤蛤抽风.gif', '蛤蛤抽烟.gif', '蛤蛤捶桌.gif', '蛤蛤扶墙.jpg',
                         '蛤蛤哭.jpg', '蛤蛤拍桌.gif', '蛤蛤喝茶.gif',
                         '蛤蛤问号.gif',  '蛤蛤撞墙.gif', '好吃.gif', '射了.gif',
                        '好男人.jpg', '呵呵.jpg', '金坷垃.jpg',
                        '流泪捂脸.jpg', '啪啪啪.gif', '喷鼻血.gif', '皮卡蛤.gif',
                        '数钱.gif',  '摊手.gif', '捂脸.jpg','瞎了.jpg',
                         'SB.jpg', '被水淹没.gif', '鼻青脸肿呵呵.jpg', '持珠捂脸.jpg', '初音瞎了.jpg',
			'蛤蛤摔.jpg'];
	config.smiley_descriptions = [
                        '蛤蛤嘲讽', '蛤蛤抽风', '蛤蛤抽烟', '蛤蛤捶桌', '蛤蛤扶墙',
                         '蛤蛤哭', '蛤蛤拍桌', '蛤蛤喝茶',
                         '蛤蛤问号', '蛤蛤撞墙', '好吃', '射了',
                        '好男人', '呵呵', '金坷垃',
                        '流泪捂脸', '啪啪啪', '喷鼻血', '皮卡蛤',
                        '数钱', '摊手', '捂脸','瞎了',
                         'SB', '被水淹没', '鼻青脸肿呵呵', '持珠捂脸', '初音瞎了',
			'蛤蛤摔'];
};
