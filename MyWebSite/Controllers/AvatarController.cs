﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MyMVCWeb.Models;
using System.Web.Routing;

namespace MyMVCWeb.Controllers
{
    public class AvatarController : Controller
    {
        private UsersContext db = new UsersContext();

        const string defaultavatar = "~/Images/金馆长.jpg";
        //
        // GET: /Avatar/Show/name
        public ActionResult Show(string name=null)
        {
            var pics = db.Avatars;
            if (name == null)
            {
                MembershipUser user = Membership.GetUser();
                if (user != null)
                {

                    try
                    {
                        Pictures PicData = pics.Where(p => p.PicName == user.UserName).First();
                        return File(PicData.Data, PicData.PicType);
                    }
                    catch
                    {
                        return File(Server.MapPath(defaultavatar), "image/jpeg");
                    }

                }
                else
                {
                    return File(Server.MapPath(defaultavatar), "image/jpeg");
                    //Response.Redirect("~/Default.aspx");
                }

            }
            else
            {
                Pictures PicData = pics.FirstOrDefault(p => p.PicName == name);
                if (PicData != null)
                {
                    return File(PicData.Data, PicData.PicType);
                }
                else
                {
                    return File(Server.MapPath(defaultavatar), "image/jpeg");
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}