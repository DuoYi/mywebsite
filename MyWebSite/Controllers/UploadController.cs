﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyMVCWeb.Controllers
{
    public class UploadController : Controller
    {
        //
        // GET: /Image/
        [OutputCache(Duration=864000,VaryByParam="img", Location=System.Web.UI.OutputCacheLocation.Client)]
        public ActionResult Index(string img)
        {
            if (string.IsNullOrEmpty(img))
                return HttpNotFound();
            string path = Server.MapPath("~/Images/upload/");
            if (!System.IO.File.Exists(path + img))
                return HttpNotFound();
            if (Request.UrlReferrer == null || Request.UrlReferrer.Host.Equals(Request.Url.Host, StringComparison.OrdinalIgnoreCase))
            {
                return File(path + img, "image/jpeg");
            }
            else
            {
                path = Server.MapPath("~/Images/thumbs/");
                if (System.IO.File.Exists(path + img))
                {
                    return File(path + img, "image/jpeg");
                }
                using(var bmp = new Bitmap(200, 50)){
                    var g = Graphics.FromImage(bmp);

                    g.FillRectangle(Brushes.White, 0, 0, 200, 50);
                    g.DrawString("流量有限，请勿盗链", new Font("Arial", 12), Brushes.Red, new PointF(0, 0));
                    using(var bmpstream = new System.IO.MemoryStream()){
                        bmp.Save(bmpstream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        return File(bmpstream, "image/jpeg");
                    }
                }
            }
        }

    }
}
