﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyMVCWeb.Models;
using PagedList;

namespace MyMVCWeb.Controllers
{
    public class TopicController : Controller
    {
        public TopicController():base()
        {
            object size = System.Web.HttpContext.Current.Application["ListPagesize"];
            if (size is int)
                listpagesize = (int)size;
            else
                listpagesize = _listpagesize;
            size = System.Web.HttpContext.Current.Application["ReplyPagesize"];
            if (size is int)
                replypagesize = (int)size;
            else
                replypagesize = _listpagesize;
        }

        private const int _listpagesize = 18;
        private const int _replypagesize = 10;

        private int listpagesize;
        private int replypagesize;

        private BlogContext db = new BlogContext();

        //
        // GET: /Topic/

        public ActionResult List(int? id, int page = 1, string sort = "")
        {
            var topics = db.Topics.Include(t => t.tag);
            if (id.HasValue && id.Value > 0)
            {
                if (db.Categories.Find(id.Value) == null)
                    return HttpNotFound();
                topics = topics.Where(b => b.CategoryID == id);
            }
            ViewBag.DateSortParm = String.IsNullOrEmpty(sort) ? "Date" : "";
            ViewBag.VisitSortParm = sort == "Visit_desc" ? "Visit" : "Visit_desc";
            ViewBag.UpdateSortParm = sort == "Update_desc" ? "Update" : "Update_desc";
            ViewBag.PostSortParm = sort == "Post_desc" ? "Post" : "Post_desc";
            ViewBag.CurrentSort = sort;
            topics = BlogUtil.getSortedQuery(topics, sort);

            int pagecount = (int)Math.Ceiling((double)topics.Count() / listpagesize);
            int pagenumber;
            if (page > 0 && page <= pagecount)
            {
                pagenumber = page;
            }
            else
            {
                pagenumber = 1;
            }
            return View(topics.ToPagedList(pagenumber, listpagesize));
        }

        //
        // GET: /Topic/Details/5

        public ActionResult Details(int id = 0)
        {
            Topic topic = db.Topics.Find(id);
            if (topic == null)
            {
                return HttpNotFound();
            }
            var model = new TopicDisplay();
            model.topic = topic;
            model.blogs = db.BlogsInTopics.Where(t => t.TopicID == topic.TopicID).OrderBy(t=>t.BlogOrder).Select(t => t.blog).ToList();
            //model.blogs.Reverse();
            topic.TopicVisit++;
            db.SaveChanges();
            return View(model);
        }

        //
        // GET: /Topic/Create
        [Authorize(Roles = "Writers,Administrator,Moderator")]
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName");
            return View();
        }

        //
        // POST: /Topic/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="Writers,Administrator,Moderator")]
        [ValidateInput(false)]
        public ActionResult Create(TopicEdit topic)
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName");
            if (ModelState.IsValid)
            {
                Topic ntopic = new Topic();
                int ret = BlogUtil.CheckBlogTag(topic.TagName, 1);
                if (ret != 0)
                {
                    ModelState.AddModelError("", ret > 0 ? "专题标签只能有1个" : "标签不得超过20个字符");
                    return View(topic);
                }
                if (!BlogUtil.checkContentTags(topic.Content))
                {
                    ModelState.AddModelError("", "不许内嵌代码！警察叔叔就是这个人！！！！");
                    return View(topic);
                }
                ntopic.Author = User.Identity.Name;
                ntopic.CategoryID = topic.CategoryID;
                ntopic.Content = topic.Content;
                ntopic.CreateDate = DateTime.Now;
                ntopic.UpdateDate = DateTime.Now;
                ntopic.TopicVisit = 0;
                ntopic.TopicTitle = topic.TopicTitle;
                try
                {
                    var tag = db.Tags.SingleOrDefault(t => t.TagName == topic.TagName);
                    if (tag == null)
                    {
                        tag = new Tag { TagName = topic.TagName };
                    }
                    ntopic.tag = tag;
                    db.Topics.Add(ntopic);
                    int i = 0;
                    foreach (var id in topic.blogIDs)
                    {
                        Blog b = db.Blogs.Find(id);
                        if (b == null)
                        {
                            ModelState.AddModelError("", "未找到ID编号为"+id+"的资源");
                            return View(topic);
                        }
                        var blogintopic = new BlogsInTopic { blog = b, topic = ntopic, BlogOrder=i++ };
                        db.BlogsInTopics.Add(blogintopic);
                    }
                    if (topic.TopicImage != null)
                    {   //ValidateFileAttribute里已经检查过了
                        ntopic.isLocalImg = true;
                        string filename = BlogUtil.saveImage(topic.TopicImage);
                        ntopic.ImagePath = filename;
                        BlogUtil.savethumb(filename);
                    }
                    else
                    {
                        string imgname = BlogUtil.getFirstImg(ntopic.Content);
                        if (imgname == null || imgname.Length < 5)
                        {
                            ModelState.AddModelError("", "请添加预览图！（上传或在文中外链图片）");
                            return View(topic);
                        }
                        ntopic.isLocalImg = false;
                        ntopic.ImagePath = imgname;
                    }
                    db.SaveChanges();
                    ExpUtil.addExpOnPass(User.Identity.Name, HttpContext.Application["ExpAddOnPass"]);
                }
                catch
                {
                    if (ntopic.isLocalImg)
                        BlogUtil.removeFiles(new string[] {ntopic.ImagePath}, true);
                    throw;
                }
                return RedirectToAction("Details", new { id = ntopic.TopicID });
            }
            return View(topic);
        }

        //
        // GET: /Topic/Edit/5
        [Authorize(Roles = "Writers,Administrator,Moderator")]
        public ActionResult Edit(int id = 0)
        {
            Topic topic = db.Topics.Find(id);
            if (topic == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = id;
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", topic.CategoryID);
            return View(new TopicEdit(topic));
        }

        //
        // POST: /Topic/Edit/5

        [HttpPost]
        [Authorize(Roles = "Writers,Administrator,Moderator")]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(int id, TopicEdit etopic)
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", etopic.CategoryID);
            int ret = BlogUtil.CheckBlogTag(etopic.TagName, 1);
            if (ret != 0)
            {
                ModelState.AddModelError("", ret > 0 ? "专题标签只能有1个" : "标签不得超过20个字符");
            }
            else if (!BlogUtil.checkContentTags(etopic.Content))
            {
                ModelState.AddModelError("", "不许内嵌代码！");
            }
            else if (ModelState.IsValid)
            {
                var topic = db.Topics.Find(id);
                bool uploadsaved = false;
                var blogcurrent = db.BlogsInTopics.Where(bi => bi.TopicID == id);
                foreach (var blog in blogcurrent)
                    db.BlogsInTopics.Remove(blog);
                int i = 0;
                foreach (var bid in etopic.blogIDs)
                {
                    Blog b = db.Blogs.Find(bid);
                    if (b == null)
                    {
                        ModelState.AddModelError("", "未找到ID编号为" + id + "的资源");
                        return View(topic);
                    }
                    BlogsInTopic bit = new BlogsInTopic { blog = b, topic = topic, BlogOrder = i++ };
                    db.BlogsInTopics.Add(bit);
                }

                if (topic.tag.TagName != etopic.TagName)
                {
                    var tag = db.Tags.SingleOrDefault(t => t.TagName == etopic.TagName);
                    if (tag == null)
                    {
                        tag = new Tag { TagName = etopic.TagName };
                    }
                    topic.tag = tag;
                }
                try
                {
                    if (etopic.TopicImage != null)
                    {
                        if (topic.isLocalImg)
                        {
                            BlogUtil.removeFiles(new string[] { topic.ImagePath }, true);
                        }
                        topic.isLocalImg = true;
                        string filename = BlogUtil.saveImage(etopic.TopicImage);
                        topic.ImagePath = filename;
                        BlogUtil.savethumb(filename);
                        uploadsaved = true;
                    }
                    else if (!topic.isLocalImg)
                    {
                        string imgname = BlogUtil.getFirstImg(etopic.Content);
                        if (imgname == null || imgname.Length < 5)
                        {
                            ModelState.AddModelError("", "请添加预览图！（上传或在文中外链图片）");
                            return View(topic);
                        }
                        topic.isLocalImg = false;
                        topic.ImagePath = imgname;
                    }
                    topic.UpdateDate = DateTime.Now;
                    topic.TopicTitle = etopic.TopicTitle;
                    topic.CategoryID = etopic.CategoryID;
                    topic.Content = etopic.Content;
                    db.Entry(topic).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch
                {
                    if(uploadsaved)
                        BlogUtil.removeFiles(new string[] { topic.ImagePath }, true);
                    throw;
                }
                return RedirectToAction("Details", new { id = id });
            }
            return View(etopic);
        }
        [HttpPost]
        [Authorize]
        public JsonResult GetTitle(string id)
        {
            int blogid;
            if (!int.TryParse(id, out blogid) || blogid <= 0)
            {
                return Json(string.Empty);
            }
            Blog b = db.Blogs.Find(blogid);
            if (b == null)
            {
                return Json(string.Empty);
            }
            else
            {
                return Json(b.BlogTitle);
            }

        }
        [HttpPost]
        [Authorize]
        public ActionResult Delete(int? id = 0)
        {
            Topic topic = db.Topics.Find(id);
            if (topic == null || (!BlogUtil.checkAdmin() && topic.Author!= User.Identity.Name ))
            {
                return HttpNotFound();
            }
            db.Topics.Remove(topic);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [ValidateInput(false)]
        [Authorize]
        public ActionResult Report(string topicid, string postid, string MsgContent, string pagenum)
        {
            int bid, pid;
            if (int.TryParse(topicid, out bid))
            {
                string pageinfo = null;
                string hashtag = null;
                if (int.TryParse(postid, out pid))
                {
                    hashtag = "#listpost" + pid;
                    pageinfo = "  （页" + pagenum + "）";
                }
                string url = Url.Action("Details", new { id = bid }) + hashtag;
                string content = Server.HtmlEncode(MsgContent) + "<br>地址：<br><a href=" + url + '>' + url + "</a>" + pageinfo;
                BlogUtil.AddPost(-1, User.Identity.Name, content);
                //BlogUtil.AddMsg(User.Identity.Name, "admin", "汇报问题", content);
                return RedirectToAction("Details", new { id = bid });
            }
            return HttpNotFound();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}