﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyMVCWeb.Models;
using PagedList;

namespace MyMVCWeb.Controllers
{
    public class SearchController : Controller
    {
        public SearchController()
            : base()
        {
            object size = System.Web.HttpContext.Current.Application["ListPagesize"];
            if (size is int)
                pagesize = (int)size;
            else
                pagesize = _pagesize;
        }
        private BlogContext db = new BlogContext();
        private const int _pagesize = 30;
        private int pagesize;
        //
        // GET: /Search/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Search/Tags/5

        public ActionResult Tags(int id=0, int page=1, string sort="")
        {
            if (id == 0)
                return RedirectToAction("Index");
            Tag tag = db.Tags.Find(id);
            if (tag == null)
            {
                return RedirectToAction("Index");
            }
            tag.TagVisit++;
            db.SaveChanges();
            var query = db.TagsInBlogs.Where(i => i.TagID == id && i.blog.isApproved == true).Select(i => i.blog);
            ViewBag.DateSortParm = String.IsNullOrEmpty(sort) ? "Date" : "";
            ViewBag.VisitSortParm = sort == "Visit_desc" ? "Visit" : "Visit_desc";
            ViewBag.PostSortParm = sort == "Post_desc" ? "Post" : "Post_desc";
            ViewBag.CurrentSort = sort;
            var model = query.Select(b => new SearchDisplay {
                itemType = 1,
                itemID = b.BlogID,
                Author = b.Author,
                Title = b.BlogTitle,
                Desc = b.Content,
                Image = b.ImagePath,
                isLocalImage = b.isLocalImg,
                Visit = b.BlogVisit,
                Posts = b.posts.Count,
                LastPost = b.posts.Max(p => p.PostDate),
                Date = b.BlogDate
            });

            var topics = db.Topics.Where(i => i.TagID == id);

            if (topics.Count() > 0)
            {
                var topicdisplay = topics.Select(t => new SearchDisplay
                {
                    itemType = 2,
                    itemID = t.TopicID,
                    Author = t.Author,
                    Title = t.TopicTitle,
                    Desc = t.Content,
                    Image = t.ImagePath,
                    isLocalImage = t.isLocalImg,
                    Visit = t.TopicVisit,
                    Posts = t.Posts.Count,
                    LastPost = t.Posts.Max(p => p.PostDate),
                    Date = t.UpdateDate
                });
                model = topicdisplay.Concat(model);
            }
            model = BlogUtil.getSortedQuery(model, sort);
            int count = query.Count() + topics.Count();
            ViewBag.count = count;
            if (Math.Ceiling((double)(count / (double)pagesize)) < page || page <= 0)
            {
                page = 1;
            }
            return View(model.ToPagedList(page,pagesize));
        }

        [HttpPost]
        [ValidateInput(false)]
        public RedirectToRouteResult Start(string name, string searchmethod)
        {
            if (searchmethod == "google")
            {
                return RedirectToAction("Index", new { q = name });
            }
            else if (searchmethod == "title")
            {
                return RedirectToAction("Titles", new { name = name });
            }
            else
            {
                return RedirectToAction("TagNames", new { name = name });
            }
        }

        public ActionResult TagNames(string name, int page = 1,string sort="")
        {
            if (name == null)
                return RedirectToAction("Index");
            var query = db.TagsInBlogs.Where(tt => tt.tag.TagName.ToLower().Contains(name.ToLower())).Select(i => i.blog).Where(b=>b.isApproved == true).Distinct();
            
            ViewBag.DateSortParm = String.IsNullOrEmpty(sort) ? "Date" : "";
            ViewBag.VisitSortParm = sort == "Visit_desc" ? "Visit" : "Visit_desc";
            ViewBag.PostSortParm = sort == "Post_desc" ? "Post" : "Post_desc";
            ViewBag.CurrentSort = sort;
            var model = query.Select(b => new SearchDisplay
            {
                itemType = 1,
                itemID = b.BlogID,
                Author = b.Author,
                Title = b.BlogTitle,
                Desc = b.Content,
                Image = b.ImagePath,
                isLocalImage = b.isLocalImg,
                Visit = b.BlogVisit,
                Posts = b.posts.Count,
                LastPost = b.posts.Max(p => p.PostDate),
                Date = b.BlogDate
            });

            var tags = db.Tags.Where(tt => tt.TagName.ToLower().Contains(name.ToLower())).Distinct().OrderByDescending(i => i.TagVisit);
            ViewBag.tags = tags;
            var topics = db.Topics.Where(t => tags.Contains(t.tag));//Join(tags, topic => topic.TagID, tag => tag.TagID, (topic, tag) => topic);

            if (topics.Count() > 0)
            {
                var topicdisplay = topics.Select(t => new SearchDisplay
                {
                    itemType = 2,
                    itemID = t.TopicID,
                    Author = t.Author,
                    Title = t.TopicTitle,
                    Desc = t.Content,
                    Image = t.ImagePath,
                    isLocalImage = t.isLocalImg,
                    Visit = t.TopicVisit,
                    Posts = t.Posts.Count,
                    LastPost = t.Posts.Max(p => p.PostDate),
                    Date = t.UpdateDate
                });
                model = topicdisplay.Concat(model);
            }
            model = BlogUtil.getSortedQuery(model, sort);
            int count = query.Count() + topics.Count();
            ViewBag.name = name;
            ViewBag.count = count;
            if (page != 1)
            {
                if (Math.Ceiling((double)(count / (double)pagesize)) < page || page <= 0)
                {
                    page = 1;
                }
            }
            else
            {
                int titlecount = db.Blogs.Count(b => b.isApproved == true && b.BlogTitle.ToLower().Contains(name.ToLower()))
                            + db.Topics.Count(t => t.TopicTitle.ToLower().Contains(name.ToLower()));
                ViewBag.titlecount = titlecount;
            }
            return View("Tags", model.ToPagedList(page, pagesize));
        }

        public ActionResult Titles(string name, int page = 1,string sort="") 
        {
            if (name == null)
                return RedirectToAction("Index");
            var query = db.Blogs.Where(b => b.isApproved == true && b.BlogTitle.ToLower().Contains(name.ToLower()));
            ViewBag.name = name;
            ViewBag.DateSortParm = String.IsNullOrEmpty(sort) ? "Date" : "";
            ViewBag.VisitSortParm = sort == "Visit_desc" ? "Visit" : "Visit_desc";
            ViewBag.PostSortParm = sort == "Post_desc" ? "Post" : "Post_desc";
            ViewBag.CurrentSort = sort;
            var model = query.Select(b => new SearchDisplay
            {
                itemType = 1,
                itemID = b.BlogID,
                Author = b.Author,
                Title = b.BlogTitle,
                Desc = b.Content,
                Image = b.ImagePath,
                isLocalImage = b.isLocalImg,
                Visit = b.BlogVisit,
                Posts = b.posts.Count,
                LastPost = b.posts.Max(p => p.PostDate),
                Date = b.BlogDate
            });
            var topics = db.Topics.Where(t => t.TopicTitle.ToLower().Contains(name.ToLower()));
            if (topics.Count() > 0)
            {
                var topicdisplay = topics.Select(t => new SearchDisplay
                {
                    itemType = 2,
                    itemID = t.TopicID,
                    Author = t.Author,
                    Title = t.TopicTitle,
                    Desc = t.Content,
                    Image = t.ImagePath,
                    isLocalImage = t.isLocalImg,
                    Visit = t.TopicVisit,
                    Posts = t.Posts.Count,
                    LastPost = t.Posts.Max(p => p.PostDate),
                    Date = t.UpdateDate
                });
                model = topicdisplay.Concat(model);
            }
            model = BlogUtil.getSortedQuery(model, sort);
            int count = query.Count() + topics.Count();
            ViewBag.count = count;
            if (page != 1)
            {
                if (Math.Ceiling((double)(count / (double)pagesize)) < page || page <= 0)
                {
                    page = 1;
                }
            }
            else
            {
                int tagcount = db.Tags.Where(tt => tt.TagName.ToLower().Contains(name.ToLower())).Distinct().Count();
                ViewBag.tagcount = tagcount;
            }
            return View(model.ToPagedList(page, pagesize)); 
        }

        [HttpPost]
        public JsonResult FetchTags()
        {
            var query = db.Tags.OrderByDescending(t => t.TagVisit).Take(200);
            return Json(query.Select(t => t.TagName).ToArray());
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
