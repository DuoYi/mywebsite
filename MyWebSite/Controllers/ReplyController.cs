﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyMVCWeb.Models;
using PagedList;

namespace MyMVCWeb.Controllers
{
    public class ReplyController : Controller
    {
        public ReplyController()
            : base()
        {
            object size = System.Web.HttpContext.Current.Application["ReplyPagesize"];
            if (size is int)
                replypagesize = (int)size;
            else
                replypagesize = _replypagesize;
        }
        private int replypagesize;
        private const int _replypagesize = 10;
        private BlogContext db = new BlogContext();


        [ValidateInput(false)]
        [HttpPost]
        [Authorize]
        public ActionResult AddReply(string itemid, string addreplycontent, string Captcha, string Prefix, string idtype = "1")
        {
            int id = int.Parse(itemid);
            //dynamic viewModel = new ExpandoObject();
            var viewModel = id;
            if (BlogUtil.checkCaptchaError(Captcha, Prefix))
            {
                ModelState.AddModelError("Captcha", "验证码计算错误，请重试。");
                return PartialView(viewModel);
            }
            DateTime? lastpost = Session["LastPostTime"] as DateTime?;
            if (lastpost.HasValue)
            {
                var diff = DateTime.Now - lastpost.Value;
                if (diff.TotalSeconds < 30)
                {
                    ModelState.AddModelError("添加回复", "回复CD为30秒，请等" + (30 - diff.Seconds) + "秒后再试");
                    return PartialView(viewModel);
                }
            }
            if (string.IsNullOrWhiteSpace(BlogUtil.removeAllTags(addreplycontent)))
            {
                ModelState.AddModelError("添加回复", "回复不能为空");
                return PartialView(viewModel);
            }
            else if (!BlogUtil.checkContentTags(addreplycontent))
            {
                ModelState.AddModelError("添加回复", "不许内嵌代码！！！");
                return PartialView(viewModel);
            }

            Post np = new Post();
            int i;
            if (idtype == "2")
                i = BlogUtil.AddTopicPost(id, User.Identity.Name, addreplycontent);
            else
                i = BlogUtil.AddPost(id, User.Identity.Name, addreplycontent);
            Session["LastPostTime"] = DateTime.Now;
            return Json(new { redirect = "#listpost" + i });
        }

        [ValidateInput(false)]
        [HttpPost]
        [Authorize]
        public JsonResult AddPostReply(string postid, string addreplycontent)
        {
            int id = int.Parse(postid);
            //dynamic viewModel = new ExpandoObject();
            var viewModel = id;
            DateTime? lastpost = Session["LastPostTime"] as DateTime?;
            if (lastpost.HasValue)
            {
                var diff = DateTime.Now - lastpost.Value;
                if (diff.TotalSeconds < 30)
                {
                    return Json(new { errmsg = "回复CD为30秒，请等" + (30 - diff.Seconds) + "秒后再试" });
                }
            }
            if (string.IsNullOrWhiteSpace(BlogUtil.removeAllTags(addreplycontent)))
            {
                return Json(new { errmsg = "回复不能为空" });
            }
            else if (!BlogUtil.checkContentTags(addreplycontent))
            {
                return Json(new { errmsg = "不许内嵌代码！！！" });
            }

            Post np = new Post();
            int i = BlogUtil.AddPostReply(id, User.Identity.Name, addreplycontent);
            Session["LastPostTime"] = DateTime.Now;
            return Json(new { id = i });
        }

        [HttpPost]
        public JsonResult DeleteReply(string replyid)
        {

            int id = int.Parse(replyid);
            var post = db.Replies.Find(id);
            if (post == null)
                return Json(new { errmsg = "无效的id" });
            if (post.Author != User.Identity.Name && !User.IsInRole("Administrator") && !User.IsInRole("Moderator"))
            {
                return Json(new { errmsg = "你tmd没权限删除" });
            }
            db.Replies.Remove(post);
            db.SaveChanges();
            return Json(true);
        }

        [HttpGet]
        public ActionResult ShowReply(string itemid, string idtype = "1")
        {
            int id = int.Parse(itemid);
            int num = 1;
            List<Post> posts;
            if (idtype == "2")
            {
                var blog = db.Topics.Find(id);
                if (blog == null)
                    return HttpNotFound();
                posts = blog.Posts;
            }
            else
            {
                var blog = db.Blogs.Find(id);
                if (blog == null)
                    return HttpNotFound();
                posts = blog.posts;
            }
            posts.Reverse();
            ViewData["idtype"] = idtype;
            ViewData["itemid"] = itemid;
            return PartialView(posts.ToPagedList(num, replypagesize));
        }
        [HttpPost]
        public ActionResult ShowReply(string itemid, string pagenum, string idtype = "1")
        {
            int pagesize = replypagesize;
            int id = int.Parse(itemid);
            int num = int.Parse(pagenum);
            IEnumerable<Post> posts;
            if(idtype == "2"){
                var blog = db.Topics.Find(id);
                if (blog == null)
                    return HttpNotFound();
                posts = blog.Posts;
            }
            else{
                var blog = db.Blogs.Find(id);
                if (blog == null)
                    return HttpNotFound();
                posts = blog.posts;
            }
            posts.Reverse();
            ViewData["idtype"] = idtype;
            ViewData["itemid"] = itemid;
            int pagecount = (int)Math.Ceiling((double)posts.Count() / pagesize);
            if (num <= 0 || num > pagecount)
            {
                num = 1;
            }
            return PartialView(posts.ToPagedList(num, pagesize));
        }

        [HttpGet]
        public ActionResult ShowUserReply(string itemid, string name, string idtype = "1")
        {
            int pagesize = replypagesize;
            int id = int.Parse(itemid);
            int num = 1;
            IEnumerable<Post> posts;
            if (idtype == "2")
            {
                var blog = db.Topics.Find(id);
                if (blog == null)
                    return HttpNotFound();
                posts = blog.Posts.Where(p => p.Author == name);
            }
            else
            {
                var blog = db.Blogs.Find(id);
                if (blog == null)
                    return HttpNotFound();
                posts = blog.posts.Where(p => p.Author == name);
            }
            posts.Reverse();
            ViewData["idtype"] = idtype;
            ViewData["itemid"] = itemid;
            ViewBag.relplyusername = name;
            return PartialView("ShowReply", posts.ToPagedList(num, pagesize));
        }
        [HttpPost]
        public ActionResult ShowUserReply(string itemid, string name, string pagenum, string idtype = "1")
        {
            int pagesize = replypagesize;
            int id = int.Parse(itemid);
            int num = int.Parse(pagenum);
            IEnumerable<Post> posts;
            if (idtype == "2")
            {
                var blog = db.Topics.Find(id);
                if (blog == null)
                    return HttpNotFound();
                posts = blog.Posts.Where(p => p.Author == name);
            }
            else
            {
                var blog = db.Blogs.Find(id);
                if (blog == null)
                    return HttpNotFound();
                posts = blog.posts.Where(p => p.Author == name);
            }
            posts.Reverse();
            ViewData["idtype"] = idtype;
            ViewData["itemid"] = itemid;
            int pagecount = (int)Math.Ceiling((double)posts.Count() / pagesize);
            if (num <= 0 || num > pagecount)
            {
                num = 1;
            }
            return PartialView("ShowReply", posts.ToPagedList(num, pagesize));
        }

        [HttpPost]
        [ValidateInput(false)]
        [Authorize]
        public ActionResult EditReply(string PostId, string content)
        {
            int id = int.Parse(PostId);
            var post = db.Posts.Find(id);
            if (post == null)
                return HttpNotFound();
            if (post.Author != User.Identity.Name && !User.IsInRole("Administrator") && !User.IsInRole("Moderator"))
            {
                return Json(new { errmsg = "你tmd没权限更改" });
            }
            else if (string.IsNullOrWhiteSpace(content))
            {
                return Json(new { errmsg = "回复不能为空" });
            }
            else if (!BlogUtil.checkContentTags(content))
            {
                return Json(new { errmsg = "不许内嵌代码！！！" });
            }
            post.Content = content;
            db.SaveChanges();
            return Json(true);
        }
        [HttpPost]
        public ActionResult DeletePost(string PostId)
        {
            int id = int.Parse(PostId);
            var post = db.Posts.Find(id);
            if (post == null)
                return HttpNotFound();
            if (post.Author != User.Identity.Name && !User.IsInRole("Administrator") && !User.IsInRole("Moderator"))
            {
                return Json(new { errmsg = "你tmd没权限删除" });
            }
            db.Posts.Remove(post);
            db.SaveChanges();
            return Json(true);
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
