﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using MyMVCWeb.Models;
using Newtonsoft.Json;
using PagedList;
using WebMatrix.WebData;

namespace MyMVCWeb.Controllers
{
    public class HomeController : Controller
    {
        public HomeController():base()
        {
            object size = System.Web.HttpContext.Current.Application["HomePagesize"];
            if (size is int && (int)size > 0)
                homepagesize = (int)size;
            else
                homepagesize = _homepagesize;
            size = System.Web.HttpContext.Current.Application["UserPagesize"];
            if (size is int)
                userpagesize = (int)size;
            else
                userpagesize = _homepagesize;
        }
        const int _homepagesize = 8;
        private int homepagesize;
        private int userpagesize;

        private BlogContext db = new BlogContext();
        public ActionResult Index([QueryString] int? page)
        {
            int pagesize = homepagesize;
            int pagenumber;
            var query = db.Blogs.Where(b => b.isApproved == true);
            int pagecount = (int)Math.Ceiling((double)query.Count() / pagesize);
            if (page.HasValue && page.Value > 0 && page.Value <= pagecount)
            {
                pagenumber = page.Value;
            }
            else
            {
                pagenumber = 1;
            }

            List<int> featuredBlogId = HttpContext.Application["FeaturedBlogId"] as List<int>;
            if (featuredBlogId != null && featuredBlogId.Count > 0)
            {
                ViewBag.featuredBlog = featuredBlogId;
                query = query.OrderByDescending(b => featuredBlogId.Contains(b.BlogID)).ThenByDescending(b => b.BlogDate);
            }
            else
                query = query.OrderByDescending(b => b.BlogDate);
            
            return View(query.ToPagedList(pagenumber, pagesize));
        }

        public ActionResult UserInfo(string name = "", int page=1)
        {
            if (name.Length < 1)
            {
                if (!User.Identity.IsAuthenticated)
                    return RedirectToAction("Index");
                name = User.Identity.Name;
            }
            using (var udb = new UsersContext())
            {
                int pagesize = userpagesize;
                UserProfile p = udb.UserProfiles.SingleOrDefault(u => u.UserName == name);
                if (p == null)
                    return new HttpNotFoundResult("用户不存在");
                ViewBag.UserBlogs = db.Blogs.Count(b => b.Author == p.UserName);
                ViewBag.UserPosts = db.Posts.Count(b => b.Author == p.UserName);
                IQueryable<Blog> query = query = db.Blogs.Where(b => b.Author == name && b.isApproved == true).OrderByDescending(b => b.BlogDate);
                int pagecount = (int)Math.Ceiling((double)query.Count() / pagesize);
                if (page <= 0 || page > pagecount)
                {
                    page = 1;
                }
                ViewBag.UserBlogItems = query.ToPagedList(page, pagesize);
                return View(p);
            }
        }

        public ActionResult Suggestions()
        {
            var blog = db.Blogs.Find(0);
            ViewBag.versiondata = blog.Content;
            ViewBag.versiontitle = blog.BlogTitle;
            return View();
        }

        public ActionResult Msg(string name)
        {
            if (string.IsNullOrEmpty(name))
                return RedirectToAction("Index", "Home");
            else
            {
                TempData["WriteAction"] = WriteAction.write;
                TempData["WriteTo"] = name;
                TempData["DisplayTab"] = "write";
                return RedirectToAction("Index", "Message");
            }
        }

        [Authorize]
        public ActionResult UnApprove(int page=1)
        {
            int pagesize = userpagesize;
            string name = User.Identity.Name;
            var query = db.Blogs.Where(b => b.Author == name && (b.isApproved == false || b.isApproved == null)).OrderByDescending(b => b.BlogDate);
            int pagecount = (int)Math.Ceiling((double)query.Count() / pagesize);
            if (page <= 0 || page > pagecount)
            {
                page = 1;
            }
            return View(query.ToPagedList(page, pagesize));
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public JsonResult SignIn()
        {
            using (var userdb = new UsersContext())
            {
                var profile = userdb.UserProfiles.Find(WebSecurity.CurrentUserId);
                TimeSpan diffday = DateTime.Now - profile.LastSignDate;
                if (diffday.Days == 1)
                {
                    profile.ConsecutiveSign++;
                }
                else if (diffday.Days < 1)
                {
                    return Json(new { success = false });
                }
                else
                    profile.ConsecutiveSign = 1;
                profile.LastSignDate = DateTime.Now.Date;
                List<int> days = HttpContext.Application["ExpAddOnDay"] as List<int>;
                int exp = 0;
                if (days != null)
                {
                    if (profile.ConsecutiveSign > days.Count)
                    {
                        exp = days.Last();
                        ExpUtil.addExp(User.Identity.Name, exp);
                    }
                    else
                    {
                        exp = days[profile.ConsecutiveSign - 1];
                        ExpUtil.addExp(User.Identity.Name, exp);
                    }
                }
                userdb.SaveChanges();
                return Json(new { success=true, days = profile.ConsecutiveSign, exp = exp });
            }
        }
        [HttpPost]
        public JsonResult GetNew()
        {
            List<int> countlist = new List<int>();
            int count = 0;
            int[] categorycount = db.Categories.Select(c => c.CategoryID).ToArray();
            foreach(int i in categorycount)
            {
                count = db.Blogs.Count(b => b.CategoryID == i && b.isApproved == true && System.Data.Objects.EntityFunctions.DiffDays(b.BlogDate, DateTime.Now) <= 1);
                countlist.Add(count);
            }
            return Json(countlist);
        }

        [ChildActionOnly]
        public PartialViewResult Header()
        {
            List<Category> categories = db.Categories.ToList();
            List<HeaderDisplay> model = new List<HeaderDisplay>(categories.Count);
            int count = 0;
            foreach (var c in categories)
            {
                count = db.Blogs.Count(b => b.CategoryID == c.CategoryID && b.isApproved == true && System.Data.Objects.EntityFunctions.DiffDays(b.BlogDate, DateTime.Now) <= 1);
                model.Add(new HeaderDisplay { category= c, newItems = count});
            }
            return PartialView("_Category", model);
        }

        [ChildActionOnly]
        public PartialViewResult Ranking()
        {
            string rankdata= System.IO.File.ReadAllText(Server.MapPath("~/Scripts/ranking.js"));
            var rankings = JsonConvert.DeserializeObject<IEnumerable<Ranking>>(rankdata);
            return PartialView("RankingPartial", rankings);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
