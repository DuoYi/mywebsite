﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using MyMVCWeb.Models;
using PagedList;

namespace MyMVCWeb.Controllers
{
    public class BlogController : Controller
    {
        public BlogController():base()
        {
            object size = System.Web.HttpContext.Current.Application["ListPagesize"];
            if (size is int)
                listpagesize = (int)size;
            else
                listpagesize = _listpagesize;
            
        }
        private BlogContext db = new BlogContext();

        private const int _listpagesize = 18;

        private int listpagesize;
        
        //
        // GET: /BlogList/

        public ActionResult List(int? id, int page=1, string sort="")
        {
            int pagesize = listpagesize;
            IQueryable<Blog> query = db.Blogs.Where(b => b.isApproved == true);
            if (id.HasValue && id.Value > 0)
            {
                if (db.Categories.Find(id.Value) == null)
                    return HttpNotFound();
                query = query.Where(b => b.CategoryID == id);
            }
            ViewBag.DateSortParm = String.IsNullOrEmpty(sort) ? "Date" : "";
            ViewBag.VisitSortParm = sort == "Visit_desc" ? "Visit" : "Visit_desc";
            ViewBag.PostSortParm = sort == "Post_desc" ? "Post" : "Post_desc";
            ViewBag.RateSortParm = sort == "Rate_desc" ? "Rate" : "Rate_desc";
            ViewBag.CurrentSort = sort;
            var model = BlogUtil.getSortedQuery(db, query, sort);
            
            int pagecount = (int)Math.Ceiling( (double) query.Count() / pagesize);
            int pagenumber;
            if (page > 0 && page <= pagecount)
            {
                pagenumber = page;
            }
            else{
                pagenumber = 1;
            }
            return View("List",model.ToPagedList(pagenumber,pagesize));
        }
        [Authorize]
        public ActionResult Create()
        {
            SelectList sl = new SelectList(db.Categories.ToList(), "CategoryID", "CategoryName");
            ViewBag.CategoryList = sl;
            return View();
        }


        [HttpPost][Authorize]
        [ValidateAntiForgeryToken][ValidateInput(false)]
        public ActionResult Create(BlogEdit blog, HttpPostedFileBase[] files, string Captcha, string Prefix)
        {
            SelectList sl = new SelectList(db.Categories.ToList(), "CategoryID", "CategoryName");
            ViewBag.CategoryList = sl;
            string content = blog.Content;
            if (BlogUtil.checkCaptchaError(Captcha, Prefix))
            {
                ModelState.AddModelError("Captcha", "验证码计算错误，请重试。");
                return View(blog);
            }
            if (content == null || string.IsNullOrWhiteSpace(BlogUtil.removeAllTags(content)))
            {
                ModelState.AddModelError("", "内容不能为空或纯图片");
                return View(blog);
            }
            if (blog.CategoryID != 1)
            {
                if (blog.BlogLinks == null)
                {
                    ModelState.AddModelError("", "链接地址不能为空");
                    return View(blog);
                }
                else
                {
                    blog.BlogLinks = blog.BlogLinks.Where(b => !string.IsNullOrWhiteSpace(b.url)).ToArray();
                    if (!BlogUtil.checkBlogLinks(blog.BlogLinks))
                    {
                        ModelState.AddModelError("", "链接地址不能为空，且不得包含javascript");
                        return View(blog);
                    }
                }
            }
            if (!BlogUtil.checkContentTags(content))
            {
                ModelState.AddModelError("", "不许内嵌代码！警察叔叔就是这个人！！！！");
                return View(blog);
            }
            int ret = BlogUtil.CheckBlogTag(blog.BlogTags);
            if (ret != 0)
            {
                ModelState.AddModelError("", ret > 0 ? "标签不得超过10个" : "标签不得超过20个字符");
                return View(blog);
            }
            content = BlogUtil.appendPassToContent(content, blog.BlogPasswords);
            string imgname = null;
            List<string> imglist = null;
            List<HttpPostedFileBase> BlogImages = null;
            bool isLocalimg = false;
            if (files != null)
            {
                int i = 0;
                BlogImages = files.ToList();
                foreach (var file in files)
                {
                    if (file != null)
                    {
                        if (!file.ContentType.Contains("image"))
                        {
                            ModelState.AddModelError("", "不接受的文件类型");
                            return View(blog);
                        }
                        else if (file.ContentLength > 1048576 * 4)
                        {
                            ModelState.AddModelError("", "文件不得超过4MB");
                            return View(blog);
                        }
                        isLocalimg = true;
                        i++;
                    }
                    else
                    {
                        BlogImages.Remove(file);
                        content = BlogUtil.removeImgPlaceholder(content, i);
                    }
                }
            }
            int addSuccess;
            try
            {
                if (!isLocalimg)
                {
                    imgname = BlogUtil.getFirstImg(content);
                    if (imgname == null || imgname.Length < 5)
                    {
                        ModelState.AddModelError("", "请添加预览图！（上传或在文中外链图片）");
                        return View(blog);
                    }
                }
                else
                {
                    imglist = BlogUtil.saveImages(BlogImages);
                    imgname = imglist.First();
                    BlogUtil.savethumb(imgname);
                    imglist.Remove(imgname);
                }
                // Add product data to DB.
                string imgpath = imgname;
                if (imglist != null)
                {
                    foreach (string name in imglist)
                    {
                        imgpath += (";" + name);
                    }
                }
                bool approve = false;
            
                if (User.IsInRole("Administrator") || User.IsInRole("Writers") || User.IsInRole("Moderator"))
                {
                    approve = true;
                    ExpUtil.addExpOnPass(User.Identity.Name, HttpContext.Application["ExpAddOnPass"]);
                }
                addSuccess = BlogUtil.AddBlog(blog.BlogTitle, content,
                    blog.CategoryID, imgpath, User.Identity.Name, approve, isLocalimg, blog.BlogLinks);
                if (!string.IsNullOrEmpty(blog.BlogTags))
                {
                    string[] tags = BlogUtil.SplitTags(blog.BlogTags);
                    BlogUtil.AddTagsForBlog(addSuccess, tags);
                }
            }
            catch 
            {
                if (isLocalimg)
                {
                    imglist.Insert(0, imgname);
                    BlogUtil.removeFiles(imglist, true);
                }
                throw;
            }
            return RedirectToAction("Details", new { id = addSuccess });
        }

        public ActionResult Details(int id=0)
        {
            Blog blog = null;
            if(id > 0)
                blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return RedirectToAction("List");
            }
            blog.BlogVisit++;
            db.SaveChanges();
            BlogDisplay bd = new BlogDisplay();
            bd.blog = blog;
            bd.tag = db.TagsInBlogs.Where(i => i.BlogID == id).Select(i => i.tag);
            foreach (var tag in bd.tag)
            {
                tag.TagVisit++;
            }
            db.SaveChanges();
            return View(bd);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator, Moderator")]
        public ActionResult Delete(int id, bool sendmsg, string MsgContent)
        {
            Blog b = db.Blogs.Find(id);
            if(sendmsg){
                BlogUtil.SendNoticeMsg(b.Author, NoticeType.delblog,User.Identity.Name, MsgContent, b.BlogTitle);
            }
            BlogUtil.deleteBlog(id);
            return RedirectToAction("List");
        }
        
        [HttpPost]
        [Authorize]
        public ActionResult Delete(int? blogid)
        {
            if (blogid.HasValue)
            {
                Blog b = db.Blogs.Find(blogid.Value);
                if (b != null)
                {
                    if (b.Author == User.Identity.Name || User.IsInRole("Administrator") || User.IsInRole("Moderator"))
                    {
                        BlogUtil.deleteBlog(b.BlogID);
                        return RedirectToAction("List");
                    }
                }
            }
            HttpContext.Response.StatusCode = 403;
            HttpContext.Response.End();
            return null;
            //throw new InvalidOperationException("Invalid Blog ID, or authorize error.");
        }
        [Authorize]
        public ActionResult Edit(int? id)
        {
            Blog blog = null;
            if (id.HasValue)
                blog = db.Blogs.Find(id.Value);
            SelectList sl = new SelectList(db.Categories.ToList(), "CategoryID", "CategoryName", blog.CategoryID);
            ViewBag.CategoryList = sl;
            if (blog == null)
            {
                return HttpNotFound();
            }
            if (blog.Author != User.Identity.Name && !User.IsInRole("Administrator") && !User.IsInRole("Moderator"))
            {
                return RedirectToAction("Details", new { id = id });
            }
            ViewBag.id = id.Value;
            var edit = new BlogEdit(blog);
            return View(edit);
        }
        [HttpPost]
        [ValidateInput(false)]
        [Authorize][ValidateAntiForgeryToken]
        public ActionResult Edit(int id, BlogEdit blog, HttpPostedFileBase[] files, bool checkbox1=false)
        {
            SelectList sl = new SelectList(db.Categories.ToList(), "CategoryID", "CategoryName", blog.CategoryID);
            ViewBag.CategoryList = sl;
            ViewBag.id = id;
            if (blog.CategoryID != 1)
            {
                if (blog.BlogLinks == null)
                {
                    ModelState.AddModelError("", "链接地址不能为空");
                    return View(blog);
                }
                else
                {
                    blog.BlogLinks = blog.BlogLinks.Where(b => !string.IsNullOrWhiteSpace(b.url)).ToArray();
                    if (!BlogUtil.checkBlogLinks(blog.BlogLinks))
                    {
                        ModelState.AddModelError("", "链接地址不能为空，且不得包含javascript");
                        return View(blog);
                    }
                }
            }
            int ret = BlogUtil.CheckBlogTag(blog.BlogTags);
            if (ret != 0)
            {
                ModelState.AddModelError("", ret > 0 ? "标签不得超过10个" : "标签不得超过20个字符");
            }
            if (blog.Content == null || string.IsNullOrWhiteSpace(BlogUtil.removeAllTags(blog.Content)))
            {
                ModelState.AddModelError("", "内容不能为空或纯图片");
                return View(blog);
            }
            if (!BlogUtil.checkContentTags(blog.Content))
            {
                ModelState.AddModelError("", "不许内嵌代码！");
            }
            else if (ModelState.IsValid)
            {
                Blog originalblog = db.Blogs.Find(id);
                bool hasupload = false;
                List<string> dellist = null;
                List<string> newlist = null;
                string thumb = null;
                string newthumb = null;
                int imgcount = 0;
                int oimgcount = 0;
                bool[] uploadpos = null;
                if(files!=null)
                    uploadpos = new bool[files.Length];
                if (originalblog.isLocalImg)
                {
                    dellist = originalblog.ImagePath.Split(';').ToList();
                    thumb = dellist.First();
                    //newthumb = thumb;
                }
                if (blog.ImagePath != null && blog.ImagePath.Length != 0) //item has local image & might or might not changed
                {
                    if (!BlogUtil.checkFieldValid(originalblog.ImagePath, blog.ImagePath))
                    {
                        ModelState.AddModelError("", "内部参数错误，请刷新重试");
                        return View(blog);
                    }
                    dellist = BlogUtil.checkDeletedImgs(originalblog.ImagePath, blog.ImagePath);
                    originalblog.isLocalImg = true;
                    imgcount = blog.ImagePath.Split(';').Count();
                    oimgcount = imgcount;
                    originalblog.ImagePath = blog.ImagePath;
                }
                if (files != null)
                {
                    for (int i = 0; i < files.Length; i++)
                    {
                        var data = files[i];
                        if (data != null)
                        {
                            if (data.ContentLength > 1048576 * 4 || !data.ContentType.Contains("image"))
                            {
                                ModelState.AddModelError("", "单个文件不得超过4MB，且必须是图片");
                                return (View(blog));
                            }
                            if (imgcount++ >= 4)
                            {
                                ModelState.AddModelError("", "图片总数不得超过4张！");
                                return (View(blog));
                            }
                            hasupload = true;
                            uploadpos[i] = true;
                        }
                        else
                            uploadpos[i] = false;
                    }
                }
                if (hasupload) // has upload file
                {
                    int i = 0;
                    originalblog.isLocalImg = true;
                    newlist = BlogUtil.saveImages(files);
                    originalblog.ImagePath = blog.ImagePath;
                    if (checkbox1 && files[0] != null && originalblog.ImagePath != null)
                    {
                        originalblog.ImagePath = originalblog.ImagePath.Insert(0, newlist[0] + ';');
                        newlist.Remove(newlist[0]);
                        blog.Content = BlogUtil.insertImgPlaceholder(blog.Content);
                        i++;
                    }
                    blog.Content = BlogUtil.replaceNewImgPlaceholder(blog.Content, i, oimgcount, uploadpos);
                    newlist.ForEach(str => originalblog.ImagePath += ';' + str);
                    if (originalblog.ImagePath[0] == ';')
                        originalblog.ImagePath = originalblog.ImagePath.Remove(0, 1);
                }
                if (imgcount == 0) //no img no upload
                {
                    string imgname = BlogUtil.getFirstImg(blog.Content);
                    if (imgname == null || imgname.Length < 5)
                    {
                        ModelState.AddModelError("", "请添加预览图！（上传或外链图片）");
                        blog.ImagePath = originalblog.ImagePath;
                        return (View(blog));
                    }
                    originalblog.ImagePath = imgname;
                    originalblog.isLocalImg = false;
                }
                else
                {
                    newthumb = originalblog.ImagePath.Split(';').ToList().First();
                }
                blog.Content = BlogUtil.appendPassToContent(blog.Content, blog.BlogPasswords);
                // else image uploaded before and did not changed
                try
                {

                    originalblog.BlogTitle = blog.BlogTitle;
                    //string edited = "<p style='font-style:italic; font-size:smaller;'>最后由 " + Membership.GetUser().UserName + " 于 " + DateTime.Now.ToString() + "编辑。</p>" ;
                    //b.Content = content.Value + edited;
                    originalblog.Content = blog.Content;
                    originalblog.CategoryID = blog.CategoryID;
                    originalblog.Links = Newtonsoft.Json.JsonConvert.SerializeObject(blog.BlogLinks);
                    if (originalblog.isApproved == false)
                    {
                        originalblog.isApproved = (bool?)null;
                        originalblog.BlogDate = DateTime.Now;
                    }
                    db.SaveChanges();
                    if (!string.IsNullOrWhiteSpace(blog.BlogTags))
                    {
                        string[] tags = BlogUtil.SplitTags(blog.BlogTags);
                        BlogUtil.SetTagsForBlog(originalblog.BlogID, tags);
                    }
                    else
                    {
                        BlogUtil.DelTagsInBlog(originalblog.BlogID);
                    }
                }
                catch
                {
                    BlogUtil.removeFiles(newlist);
                    throw;
                }
                if (dellist != null)
                    BlogUtil.removeFiles(dellist);
                if (thumb != null && thumb != newthumb)
                    BlogUtil.removethumb(thumb);
                if (originalblog.isLocalImg)
                    BlogUtil.savethumb(originalblog.ImagePath);


                return RedirectToAction("Details", new { id = id });
            }
            return View(blog);
        }

        [HttpPost]
        public ActionResult Rate(string blogid, string rating)
        {
            int id = int.Parse(blogid);
            int value = int.Parse(rating);
            string ip = Request.UserHostAddress;
            DateTime today = DateTime.Now.Date;
            Rating rate =
            db.Ratings.SingleOrDefault(r => r.BlogID == id && r.ip == ip && r.ratetime == today);
            if (rate != null)
            {
                return Json(new { errmsg = "今天已经评过分了！" });
            }
            switch (value)
            {
                case 1:
                    value = -1;
                    break;
                case 2:
                    value = 0;
                    break;
                case 3:
                    value = 1;
                    break;
                case 4:
                    value = 3;
                    break;
                case 5:
                    value = 5;
                    break;
                default:
                    return Json(new { errmsg = "无效的评分，请刷新重试" });
            }
            rate = new Rating();
            rate.value = value;
            rate.ratetime = today;
            rate.ip = ip;
            rate.BlogID = id;
            rate.RatingID = System.Guid.NewGuid();
            db.Ratings.Add(rate);
            db.SaveChanges();
            return GetRate(blogid);
        }
        [HttpPost]
        public ActionResult GetRate(string blogid)
        {
            int id = int.Parse(blogid);
            return Json( new { rating = BlogUtil.getRating(id)});
        }

        
        [ValidateInput(false)]
        [Authorize]
        public ActionResult Report(string blogid, string postid, string MsgContent, string pagenum)
        {
            int bid,pid;
            if (int.TryParse(blogid, out bid))
            {
                string pageinfo = null;
                string hashtag = null;
                if (int.TryParse(postid, out pid))
                {
                    hashtag = "#listpost" + pid;
                    pageinfo = "  （页" + pagenum + "）";
                }
                string url = Url.Action("Details", new { id = bid }) + hashtag;
                string content = Server.HtmlEncode(MsgContent) + "<br>地址：<br><a href=" + url + '>'+url+"</a>" + pageinfo;
                BlogUtil.AddPost(-1, User.Identity.Name, content);
                //BlogUtil.AddMsg(User.Identity.Name, "admin", "汇报问题", content);
                return RedirectToAction("Details", new { id = bid });
            }
            return HttpNotFound();
        }

        [Authorize]
        [ValidateInput(false)]
        public JsonResult GetMagnet(string data)
        {
            if (string.IsNullOrEmpty(data))
                return null;
            return Json(TorrentUtil.GetMagnetURL(System.Convert.FromBase64String(data)));
        }
        [Authorize]
        [ValidateInput(false)]
        public JsonResult AddTag(string name, string blogid)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return Json(new { errmsg = "标签不能为空" });
            }
            else if (name.Length > 20)
            {
                return Json(new { errmsg = "标签不得超过20字符" });
            }
            else if (name.IndexOfAny(new char[] { '，', '。', ',', ';', '；', '、', ' ' }) >= 0)
            {
                return Json(new { errmsg = "标签不得含有逗号、句号、分号、顿号或空格" });
            }
            else
            {
                int id;
                if(!int.TryParse(blogid, out id)){
                    return Json(new { errmsg = "无效id，请刷新重试" });
                }
                Blog b = db.Blogs.Find(id);
                if (b == null)
                {
                    return Json(new { errmsg = "无效id，请刷新重试" });
                }
                if (db.TagsInBlogs.Count(t => t.BlogID == id && t.tag.TagName == name) > 0)
                {
                    return Json(new { errmsg = "标签已存在" });
                }
                if (db.TagsInBlogs.Count(t => t.BlogID == id) >= 10)
                {
                    return Json(new { errmsg = "标签不得超过10个" });
                }
                id = BlogUtil.AddTagForBlog(db, id, name);
                return Json(new { tagid = id });
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
