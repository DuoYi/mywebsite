﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using MyMVCWeb.Models;
using PagedList;
using WebMatrix.WebData;

namespace MyMVCWeb.Controllers
{
    [Authorize(Roles="Administrator, Moderator")]
    public class AdminController : Controller
    {
        private BlogContext db = new BlogContext();

        private AppSettingsModel GetAppSettings()
        {
            AppSettingsModel model = new AppSettingsModel();
            model.blockip = ConfigurationManager.AppSettings.Get("blockip");
            model.updateinterval = ConfigurationManager.AppSettings.Get("UpdateInterval");
            model.expaddondays = ConfigurationManager.AppSettings.Get("ExpAddOnDay");
            model.expaddonpass = ConfigurationManager.AppSettings.Get("ExpAddOnPass");
            model.auditpagesize = ConfigurationManager.AppSettings.Get("AuditPagesize");
            model.homepagesize = ConfigurationManager.AppSettings.Get("HomePagesize");
            model.listpagesize = ConfigurationManager.AppSettings.Get("ListPagesize");
            model.msgpagesize = ConfigurationManager.AppSettings.Get("MsgPagesize");
            model.replypagesize = ConfigurationManager.AppSettings.Get("ReplyPagesize");
            model.userpagesize = ConfigurationManager.AppSettings.Get("UserPagesize");
            model.rankingsize = ConfigurationManager.AppSettings.Get("RankingSize");
            return model;
        }
        private DataSettingsModel GetDataSettings()
        {
            var model = new DataSettingsModel();
            var idlist = HttpContext.Application["FeaturedBlogId"] as List<int>;
            if (idlist != null)
                model.featuredblogids = string.Join(",", idlist.Select(i => i.ToString()).ToArray());
            else
                model.featuredblogids = null;
            return model;
        }
        //
        // GET: /Admin/

        public ActionResult Manage()
        {
            AdminViewModel model = new AdminViewModel();
            using (var userdb = new UsersContext()){
                model.allCategory = db.Categories.ToList();
                model.Admins = AdminViewModel.getAdmins();
                model.Writers = AdminViewModel.getWriters();
                model.auditcount = db.Blogs.Where(b => b.isApproved == null && b.BlogID > 0).Count();
                model.bannedusercount = Roles.GetUsersInRole("Banned").Count();
                model.todaynewitem = db.Blogs.Where(b => EntityFunctions.DiffDays(b.BlogDate, DateTime.Now) == 0).Count();
                model.yesterdaynewitem = db.Blogs.Where(b => EntityFunctions.DiffDays(b.BlogDate, DateTime.Now) == 1).Count();
                model.totalauditcount = db.Blogs.Count();
                model.totalusercount = userdb.UserProfiles.Count();
                model.appsettings = this.GetAppSettings();
                model.datasettings = this.GetDataSettings();
            }
            //ViewBag.BanMsg = TempData["BanMsg"];
            //ViewBag.WriterMsg = TempData["WriterMsg"];
            //ViewBag.AdminMsg = TempData["AdminMsg"];
            ViewBag.CategoryMsg = TempData["CategoryMsg"];
            return View("Manage",model);
        }

        public ActionResult Audit(int page = 1, bool showunapprove=false)
        {
            int pagesize = 20;
            object size = HttpContext.Application["AuditPagesize"];
            if (size is int)
                pagesize = (int)size;
            IQueryable<Blog> query;
            if (showunapprove)
                query = db.Blogs.Where(b => b.isApproved == false && b.BlogID > 0).OrderByDescending(b => b.BlogDate);
            else
            {
                query = db.Blogs.Where(b => b.isApproved == null && b.BlogID > 0).OrderByDescending(b => b.BlogDate);
            }
            int pagecount = (int)Math.Ceiling((double)query.Count() / pagesize);
            if (page <= 0 || page <= pagecount)
            {
                page = 1;
            }
            return View(query.ToPagedList(page,pagesize));
        }

        //
        // POST: /Admin/Audit

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UnApprove(string blogid, int page=1, bool sendmsg=false,bool delete=false, string MsgContent="")
        {
            int id;
            if (int.TryParse(blogid, out id))
            {
                Blog b = db.Blogs.SingleOrDefault(blog => blog.BlogID == id);
                if (b != null)
                {
                    string msg = "["+ b.BlogID + "]";
                    if (delete)
                    {
                        BlogUtil.deleteBlog(b.BlogID);
                        msg = "删除通知";
                    }
                    else
                    {
                        b.isApproved = false;
                        db.SaveChanges();
                        msg = "审核通知";
                        string url = Url.Action("Details", "Blog", new { id = b.BlogID });
                        MsgContent = Server.HtmlEncode(MsgContent) + "<br>投稿地址：<br><a href=" + url + '>' + url + "</a>";
                    }
                    if(sendmsg)
                        BlogUtil.AddMsg(User.Identity.Name, b.Author, msg,MsgContent);
                    return RedirectToAction("Audit", new { page = page });
                }
            }
            return HttpNotFound("Invalid Blog ID");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Pass(string blogid, int page = 1)
        {
            int id;
            if (int.TryParse(blogid, out id))
            {
                Blog b = db.Blogs.SingleOrDefault(blog => blog.BlogID == id);
                if (b != null)
                {
                    b.isApproved = true;
                    ExpUtil.addExpOnPass(b.Author, HttpContext.Application["ExpAddOnPass"]);
                    db.SaveChanges();
                    return RedirectToAction("Audit", new { page = page });
                }
            }
            throw new InvalidOperationException("Invalid Blog ID");
        }

        [HttpPost]
        public ActionResult ManageCategory(string categoryid, string categoryname, string categorydesc, string action)
        {
            int id;
            Category c;
            switch (action)
            {
                case "new":
                    if (string.IsNullOrWhiteSpace(categoryname))
                    {
                        TempData["CategoryMsg"] = "请输入栏目名称";
                    }
                    else
                    {
                        c = new Category();
                        c.CategoryName = categoryname;
                        c.Description = categorydesc;
                        db.Categories.Add(c);
                        db.SaveChanges();
                        TempData["CategoryMsg"] = "添加成功";
                    }
                    break;
                case "edit":
                    if (string.IsNullOrWhiteSpace(categoryname))
                    {
                        TempData["CategoryMsg"] = "请输入栏目名称";
                    }
                    else if (int.TryParse(categoryid, out id))
                    {
                        c = db.Categories.Single(cc => cc.CategoryID == id);
                        c.CategoryName = categoryname;
                        c.Description = categorydesc;
                        db.SaveChanges();
                        TempData["CategoryMsg"] = "编辑成功";
                    }
                    break;
                case "del":
                     if (int.TryParse(categoryid, out id))
                    {
                        c = db.Categories.Single(cc => cc.CategoryID == id);
                        if (c.Blogs == null || c.Blogs.Count == 0)
                        {
                            db.Categories.Remove(c);
                            db.SaveChanges();
                            TempData["CategoryMsg"] = "删除成功";
                        }
                        else
                        {
                            TempData["CategoryMsg"] = "不可删除非空栏目";
                        }
                    }
                    break;
            }
            
            return RedirectToAction("Manage");
        }

        //
        // POST: /Admin/Edit/5

        [HttpPost]
        public string ManageWriter(string writername, string behaviour)
        {
            string msg = string.Empty;
            if (!WebSecurity.UserExists(writername))
            { 
                return "查无此人";
            }
            if (behaviour == "添加作者")
            {
                if (Roles.IsUserInRole(writername, "Writers"))
                {
                    return "此人已是作者！";
                }
                Roles.AddUserToRole(writername, "Writers");
                msg = "添加成功";
            }
            else if (behaviour == "删除作者")
            {
                if (!Roles.IsUserInRole(writername, "Writers"))
                {
                    return "此人不是作者！";
                }
                Roles.RemoveUserFromRole(writername, "Writers");
                msg = "删除成功";
            }

            return msg;
        }


        [HttpPost]
        public string ManageAdmin(string adminname, string behaviour, bool asadmin=false)
        {
            string msg = string.Empty;
            if (!WebSecurity.UserExists(adminname))
            {
                return "查无此人";
            }
            if (behaviour == "添加管理员")
            {
                if (Roles.IsUserInRole(adminname, "Administrator") || Roles.IsUserInRole(adminname, "Moderator"))
                {
                    return "此人已是管理员！";
                }
                using (var _db = new UsersContext())
                {
                if (asadmin)
                {
                    Roles.AddUserToRole(adminname, "Administrator");
                    _db.UserProfiles.Find(WebSecurity.CurrentUserId).Level = 99;
                }
                else
                {
                    Roles.AddUserToRole(adminname, "Moderator");
                    _db.UserProfiles.Find(WebSecurity.CurrentUserId).Level = 90;
                }
                _db.SaveChanges();
                }
                msg = "添加成功";
            }
            else if (behaviour == "删除管理员")
            {
                if (!Roles.IsUserInRole(adminname, "Moderator"))
                {
                    if (Roles.IsUserInRole(adminname, "Administrator"))
                    {
                        msg = "不可删除Admin";
                    }
                    else
                        msg = "此人不是管理员！";
                    return msg;
                }
                Roles.RemoveUserFromRole(adminname, "Moderator");
                using (var _db = new UsersContext())
                {
                    var user = _db.UserProfiles.Find(WebSecurity.CurrentUserId);
                    user.Level = ExpUtil.calculateUserLevel(user.UserId);
                    _db.SaveChanges();
                }
                msg = "删除成功";
            }

            return msg;
        }

        [HttpPost]
        public string ManageBan(string banname, string behaviour)
        {
            string msg = string.Empty;
            if (!WebSecurity.UserExists(banname))
            {
                return "查无此人";
            }
            if (behaviour == "封禁")
            {
                if (Roles.IsUserInRole(banname, "Banned"))
                {
                    return "此人已死";
                }
                else if (Roles.GetRolesForUser(banname).Count() > 0)
                {
                    return "不可封禁有职位的用户";
                }
                Roles.AddUserToRole(banname, "Banned");
                
                msg = "添加成功";
            }
            else if (behaviour == "解封")
            {
                if (!Roles.IsUserInRole(banname, "Banned"))
                {
                    return "此人未被封禁";
                }
                Roles.RemoveUserFromRole(banname, "Banned");
                msg = "解禁成功";
            }

            return msg;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public string DataSettings(string featuredBlogId)
        {
            List<int> idlist = new List<int>(5);
            if (!string.IsNullOrWhiteSpace(featuredBlogId))
            {
                string[] blogids = featuredBlogId.Split(',');
                int count = 0;
                foreach (var ids in blogids)
                {
                    int id;
                    if (!int.TryParse(ids, out id))
                    {
                        return "数据格式无效";
                    }
                    if (++count > 5)
                    {
                        return "置顶文章不得超过5个";
                    }
                    idlist.Add(id);
                }
            }
            string result = Newtonsoft.Json.JsonConvert.SerializeObject(idlist);
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/featuredBlogId.json");
            System.IO.File.WriteAllText(path, result);
                
            HttpContext.Application["FeaturedBlogId"] = idlist;
            return "设置成功";
        }

        [HttpPost][ValidateAntiForgeryToken]
        public ActionResult BanIP(string blockip)
        {
            blockip = blockip ?? string.Empty;
            blockip = blockip.Replace("\r\n", ";");
            blockip = blockip.Replace("\n", ";");
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
            //ConfigurationManager.AppSettings.Set("blockip", blockip);
            config.AppSettings.Settings["blockip"].Value = blockip;
            config.Save(ConfigurationSaveMode.Minimal);
            return RedirectToAction("Manage");
        }

        [HttpPost][ValidateAntiForgeryToken]
        public ActionResult ManageSettings(AppSettingsModel settings)
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
            config.AppSettings.Settings["UpdateInterval"].Value = settings.updateinterval;
            config.AppSettings.Settings["ExpAddOnPass"].Value = settings.expaddonpass;
            config.AppSettings.Settings["ExpAddOnDay"].Value = settings.expaddondays;
            config.AppSettings.Settings["AuditPagesize"].Value = settings.auditpagesize;
            config.AppSettings.Settings["HomePagesize"].Value = settings.homepagesize;
            config.AppSettings.Settings["ListPagesize"].Value = settings.listpagesize;
            config.AppSettings.Settings["ReplyPagesize"].Value = settings.replypagesize;
            config.AppSettings.Settings["MsgPagesize"].Value = settings.msgpagesize;
            config.AppSettings.Settings["UserPagesize"].Value = settings.userpagesize;
            config.AppSettings.Settings["RankingSize"].Value = settings.rankingsize;
            //ConfigurationManager.AppSettings.Set("UpdateInterval", updateinterval);
            config.Save(ConfigurationSaveMode.Minimal);
            return RedirectToAction("Manage");
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult EditVersion(string data, string version)
        {
            var blog = db.Blogs.Find(0);
            blog.Content = data;
            blog.BlogTitle = version;
            db.SaveChanges();
            return Json(true);
        }
    }
}
