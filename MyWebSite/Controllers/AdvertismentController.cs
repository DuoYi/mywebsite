﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyMVCWeb.Models;

namespace MyMVCWeb.Controllers
{
    public class AdvertismentController : Controller
    {
        [ChildActionOnly]
        public PartialViewResult Index()
        {
            List<Advertisment> ads;
            using (var db = new BlogContext())
            {
                var orderads = db.Advertisments.OrderByDescending(a => a.ClickCount);
                if (orderads.Count() > 0)
                {
                    db.Advertisments.First().ExposeCount++;
                    db.SaveChanges();
                    var ran = new Random();
                    ads = orderads.Take(3).ToList();
                    var restads = orderads.Skip(3).ToList();
                    ads.AddRange(restads.OrderBy(a => ran.Next()));
                }
                else
                    ads = null;
            }
            return PartialView("_AdPartial",ads);
        }
        public JsonResult Click(string adid)
        {
            int id;
            if (!int.TryParse(adid, out id))
                return null;
            else
            {
                using (var db = new BlogContext())
                {
                    var ad = db.Advertisments.Find(id);
                    if (ad != null)
                    {
                        ad.ClickCount++;
                        db.SaveChanges();
                        return Json(ad.ClickCount);
                    }
                    else
                        return null;
                }
            }
        }
        [Authorize(Roles = "Administrator"), HttpGet]
        public ViewResult AdManage()
        {
            using (var db = new BlogContext())
            {
                var ads = db.Advertisments.ToList();
                ViewBag.msg = TempData["msg"];
                return View(ads);
            }
        }
        [Authorize(Roles = "Administrator"), HttpPost, ValidateAntiForgeryToken]
        public RedirectToRouteResult AdManage(string AdID, string AdUrl, string AdImg, string AdTitle, string action)
        {
            using (var db = new BlogContext())
            {
                int id;
                Advertisment a;
                switch (action)
                {
                    case "new":
                        if (string.IsNullOrWhiteSpace(AdUrl))
                        {
                            TempData["msg"] = "请输入URL";
                        }
                        else
                        {
                            a = new Advertisment();
                            a.AdTitle = AdTitle;
                            a.AdUrl = AdUrl;
                            a.ImgUrl = AdImg;
                            db.Advertisments.Add(a);
                            db.SaveChanges();
                            TempData["msg"] = "添加成功";
                        }
                        break;
                    case "edit":
                        if (string.IsNullOrWhiteSpace(AdUrl))
                        {
                            TempData["msg"] = "请输入Url";
                        }
                        else if (int.TryParse(AdID, out id))
                        {
                            a = db.Advertisments.Find(id);
                            if (a != null)
                            {
                                a.AdTitle = AdTitle;
                                a.AdUrl = AdUrl;
                                a.ImgUrl = AdImg;
                                db.SaveChanges();
                                TempData["msg"] = "编辑成功";
                            }
                            else
                            {
                                TempData["msg"] = "未找到";
                            }
                        }
                        break;
                    case "del":
                        if (int.TryParse(AdID, out id))
                        {
                            a = db.Advertisments.Find(id);
                            if (a != null)
                            {
                                db.Advertisments.Remove(a);
                                db.SaveChanges();
                                TempData["msg"] = "删除成功";
                            }
                            else
                            {
                                TempData["msg"] = "未找到";
                            }
                        }
                        break;
                }

                return RedirectToAction("AdManage");
            }
        }
    }
}
