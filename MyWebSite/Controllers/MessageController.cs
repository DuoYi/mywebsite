﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyMVCWeb.Models;
using PagedList;
using WebMatrix.WebData;

namespace MyMVCWeb.Controllers
{
    enum WriteAction
    {
        none = 0,
        reply,
        forward,
        write
    }

    [Authorize]
    public class MessageController : Controller
    {
        public MessageController()
            : base()
        {
            object size = System.Web.HttpContext.Current.Application["MsgPagesize"];
            if (size is int)
                pagesize = (int)size;
            else
                pagesize = _pagesize;
        }
        private UsersContext db = new UsersContext();
        private const int _pagesize = 20;
        private int pagesize;
        //
        // GET: /Message/

        public ActionResult Index()
        {
            string context = TempData["DisplayTab"] as string;
            if (context != null)
            {
                ViewBag.DisplayTab = context;
            }
            return View();
        }

        public ActionResult Inbox(int pagenum=1)
        {
            string username = User.Identity.Name;
            IQueryable<Message> messages = db.Messages.Where(m => m.Recipient == username && !m.IsRecipientDelete).OrderByDescending(m =>m.MsgDate);
            ViewBag.UnreadCount = db.Messages.Count(m => m.Recipient == username && !m.IsRead && !m.IsRecipientDelete);
            int pagecount = (int)Math.Ceiling((double)messages.Count() / pagesize);
            if (pagenum <= 0 || pagenum > pagecount)
            {
                pagenum = 1;
            }
            return PartialView("InboxPartial",messages.ToPagedList(pagenum,pagesize));
        }

        public ActionResult Outbox(int pagenum = 1)
        {
            string username = User.Identity.Name;
            IQueryable<Message> messages = db.Messages.Where(m => m.Sender == username && !m.IsSenderDelete).OrderByDescending(m => m.MsgDate);
            ViewBag.UnreadCount = db.Messages.Count(m => m.Recipient == username && !m.IsRead && !m.IsRecipientDelete);
            int pagecount = (int)Math.Ceiling((double)messages.Count() / pagesize);
            if (pagenum <= 0 || pagenum > pagecount)
            {
                pagenum = 1;
            }
            return PartialView("OutboxPartial", messages.ToPagedList(pagenum, pagesize));
        }
        [HttpGet]
        public ActionResult Write()
        {
            WriteAction? a = TempData["WriteAction"] as WriteAction?;
            Message m = TempData["WriteMessage"] as Message;
            string username = User.Identity.Name;
            if (a.HasValue)
            {
                switch (a.Value)
                {
                    case WriteAction.reply:
                        if (username != m.Sender)
                        {
                            m.Recipient = m.Sender;
                        }
                        m.MsgTitle = "回复：" + m.MsgTitle;
                        m.MsgContent = string.Format("\n\n---------{0}于{1}写道---------\n", m.Sender, m.MsgDate) + m.MsgContent;
                        break;

                    case WriteAction.forward:
                        m.Recipient = "";
                        m.MsgTitle = "转发：" + m.MsgTitle;
                        m.MsgContent = string.Format("\n\n---------{0}于{1}写道---------\n", m.Sender, m.MsgDate) + m.MsgContent;
                        break;
                    case WriteAction.write:
                        m = new Message();
                        m.Recipient = TempData["WriteTo"] as string;
                        break;
                }
            }
            else m = new Message();
            return PartialView("WritePartial",m);
        }

        //
        // POST: /Message/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Write(Message message, string Captcha, string Prefix)
        {
            if (BlogUtil.checkCaptchaError(Captcha, Prefix))
            {
                ModelState.AddModelError("Captcha", "验证码计算错误，请重试。");
                return PartialView("WritePartial",message);
            }
            if (ModelState.IsValid)
            {
                message.MsgDate = DateTime.Now;
                message.Sender = User.Identity.Name;
                db.Messages.Add(message);
                db.SaveChanges();
                TempData["DisplayTab"] = "outbox";
                return JavaScript(" window.location = '" + Url.Action("Index") + "'" );
            }

            return PartialView("WritePartial",message);
        }


        [HttpPost]
        public ActionResult ReadMsg(string id)
        {
            int msgid;
            int unread;
            if (int.TryParse(id, out msgid))
            {
                string username = User.Identity.Name;
                Message message = db.Messages.Find(msgid);
                unread = db.Messages.Count(m => m.Recipient == username && !m.IsRead && !m.IsRecipientDelete);
                if (message == null || message.IsRecipientDelete || !message.Recipient.Equals(User.Identity.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return HttpNotFound();
                }
                if (!message.IsRead)
                {
                    message.IsRead = true;
                    unread--;
                    db.SaveChanges();
                }
                return Json(new
                {
                    MsgId = message.MsgId,
                    MsgTitle = message.MsgTitle,
                    Sender = message.Sender,
                    Recipient = message.Recipient,
                    MsgDate = message.MsgDate,
                    MsgContent = message.MsgContent,
                    unreadcount = unread
                });
            }
            return HttpNotFound();
        }

        [Authorize(Roles = "Administrator, Moderator")]
        public ActionResult ShowAdminInbox(int pagenum = 1)
        {
            string username = "admin";
            IQueryable<Message> messages = db.Messages.Where(m => m.Recipient == username && !m.IsRecipientDelete).OrderByDescending(m => m.MsgDate);
            ViewBag.UnreadCount = db.Messages.Count(m => m.Recipient == username && !m.IsRead && !m.IsRecipientDelete);
            int pagecount = (int)Math.Ceiling((double)messages.Count() / pagesize);
            if (pagenum <= 0 || pagenum > pagecount)
            {
                pagenum = 1;
            }
            return PartialView("ReadOnlyInbox", messages.ToPagedList(pagenum, pagesize));
        }
        [Authorize(Roles="Administrator, Moderator")]
        [HttpPost]
        public ActionResult GetAdminMsg(string id)
        {
            int msgid;
            if (int.TryParse(id, out msgid))
            {
                Message message = db.Messages.Find(msgid);
                if (message == null || message.IsRecipientDelete || !message.Recipient.Equals("admin", StringComparison.OrdinalIgnoreCase))
                {
                    return HttpNotFound();
                }
                return Json(new
                {
                    MsgId = message.MsgId,
                    MsgTitle = message.MsgTitle,
                    Sender = message.Sender,
                    Recipient = message.Recipient,
                    MsgDate = message.MsgDate,
                    MsgContent = message.MsgContent,
                });
            }
            return HttpNotFound();
        }
        [HttpPost]
        public ActionResult GetMsg(string id)
        {
            int msgid;
            if (int.TryParse(id, out msgid))
            {
                Message message = db.Messages.Find(msgid);
                if (message == null || message.IsSenderDelete || !message.Sender.Equals(User.Identity.Name,StringComparison.OrdinalIgnoreCase))
                {
                    return HttpNotFound();
                }
                return Json(new
                {
                    MsgId = message.MsgId,
                    MsgTitle = message.MsgTitle,
                    Sender = message.Sender,
                    Recipient = message.Recipient,
                    MsgDate = message.MsgDate,
                    MsgContent = message.MsgContent,
                });
            }
            return HttpNotFound();
        }


        //
        // GET: /Message/Delete/5

        public ActionResult MsgAction(string msgid, string action)
        {
            int id = int.Parse(msgid);
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            switch (action)
            {
                case "删除":
                    bool isrecipient = DeleteMessage(message);
                    if (!isrecipient)
                    {
                        TempData["DisplayTab"] = "outbox";
                    }
                    return RedirectToAction("Index");
                case "回复":
                    TempData["WriteAction"] = WriteAction.reply;
                    TempData["WriteMessage"] = message;
                    TempData["DisplayTab"] = "write";
                    return RedirectToAction("Index");
                case "转发":
                    TempData["WriteAction"] = WriteAction.forward;
                    TempData["WriteMessage"] = message;
                    TempData["DisplayTab"] = "write";
                    return RedirectToAction("Index");

            }
            return HttpNotFound();
        }

        private bool DeleteMessage(Message m, bool save=true)
        {
            string username = User.Identity.Name;
            bool isrecipient = true;
            if (m.Sender == username)
            {
                m.IsSenderDelete = true;
                isrecipient = false;
            }
            if (m.Recipient == username)
            {
                m.IsRecipientDelete = true;
                isrecipient = true;
            }
            
            if (m.IsSenderDelete && m.IsRecipientDelete)
            {
                db.Messages.Remove(m);
            }
            if(save)
                db.SaveChanges();
            return isrecipient;
        }

        [HttpPost]
        public ActionResult CheckUsername(string Recipient)
        {
            if (WebSecurity.UserExists(Recipient))
            {
                return Json(true);
            }
            else
                return Json("查无此人", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MultiAct(string action, IEnumerable<string> op, string pos)
        {
            string user = User.Identity.Name;
            var objCtx = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)db).ObjectContext;
            if (pos == "inbox")
            {
                switch (action){
                    case "删除全部":
                        objCtx.ExecuteStoreCommand("Delete from Messages where IsSenderDelete='true' and Recipient={0}", user);
                        objCtx.ExecuteStoreCommand("Delete from Messages where Recipient={0} and Sender={0}", user);
                        objCtx.ExecuteStoreCommand("Update Messages set IsRecipientDelete='true' where Recipient={0}", user);
                        break;
                    case "删除所选":
                        if(op != null)
                        foreach (string opid in op)
                        {
                            int id;
                            if(int.TryParse(opid,out id)){
                                var msg = db.Messages.Find(id);
                                DeleteMessage(msg,false);
                            }
                        }
                        break;
                    case "标记已读":
                        if (op != null)
                        foreach (string opid in op)
                        {
                            int id;
                            if (int.TryParse(opid, out id))
                            {
                                var msg = db.Messages.Find(id);
                                msg.IsRead = true;
                            }
                        }
                        break;
                    case "全标已读":
                        objCtx.ExecuteStoreCommand("Update Messages set IsRead='true' where Recipient={0} and IsRead='false'", user);
                        break;
                }
                db.SaveChanges();
            }
            else if (pos == "outbox")
            {
                switch (action)
                {
                    case "删除全部":
                        objCtx.ExecuteStoreCommand("Delete from Messages where IsRecipientDelete='true' and Sender={0}", user);
                        objCtx.ExecuteStoreCommand("Delete from Messages where Recipient={0} and Sender={0}", user);
                        objCtx.ExecuteStoreCommand("Update Messages set IsSenderDelete='true' where Sender={0}", user);
                        break;
                    case "删除所选":
                        if (op != null)
                        foreach (string opid in op)
                        {
                            int id;
                            if (int.TryParse(opid, out id))
                            {
                                var msg = db.Messages.Find(id);
                                DeleteMessage(msg, false);
                            }
                        }
                        break;
                }
                db.SaveChanges();
            }
            else
                return RedirectToAction("Index");
            TempData["DisplayTab"] = pos;
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}