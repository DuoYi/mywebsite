﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using MyMVCWeb.Filters;
using MyMVCWeb.Models;
using System.Text;
using System.IO;

namespace MyMVCWeb.Controllers
{
    [Authorize]
    //[InitializeSimpleMembership]
    public class AccountController : Controller
    {
        private UsersContext db = new UsersContext();
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (BlogUtil.checkCaptchaError(model.Captcha, null))
            {
                ModelState.AddModelError("Captcha", "验证码计算错误，请重试。");
                //dispay error and generate a new captcha
                return View(model);
            }
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                if (Roles.IsUserInRole(model.UserName,"Banned"))
                {
                    WebSecurity.Logout();
                    ModelState.AddModelError("", "此账户已被封禁，如有疑问请联系管理员。");
                    return View();
                }
                UserProfile p = db.UserProfiles.Single(u => u.UserName == model.UserName);
                p.LastLoginDate = DateTime.Now;
                p.LastLoginIP = Request.UserHostAddress;
                db.SaveChanges();
                return RedirectToLocal(returnUrl);
            }

            // 如果我们进行到这一步时某个地方出错，则重新显示表单
            ModelState.AddModelError("", "提供的用户名或密码不正确。");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            var model = new RegisterModel { X = 0, Y = 0, W = 150, H = 150 };
            return View(model);
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (BlogUtil.checkCaptchaError(model.Captcha, null))
            {
                ModelState.AddModelError("Captcha", "验证码计算错误，请重试。");
                //dispay error and generate a new captcha
                return View(model);
            }
            if (ModelState.IsValid)
            {
                // 尝试注册用户
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { Email = model.Email, LastLoginIP = Request.UserHostAddress, LastLoginDate = DateTime.Now, Points = 0, ConsecutiveSign=0 });
                    
                    WebSecurity.Login(model.UserName, model.Password);

                    if (model.avatar != null)
                    {
                        using( var stream = new MemoryStream(model.avatar.ContentLength)){
                            model.avatar.InputStream.CopyTo(stream);
                        BlogUtil.AddPic(model.UserName, model.avatar.ContentType, BlogUtil.Crop(stream, model.W, model.H, model.X, model.Y));
                        }
                    }
                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // 如果我们进行到这一步时某个地方出错，则重新显示表单
            return View(model);
        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // 只有在当前登录用户是所有者时才取消关联帐户
            if (ownerAccount == User.Identity.Name)
            {
                // 使用事务来防止用户删除其上次使用的登录凭据
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // 在某些失败方案中，ChangePassword 将引发异常，而不是返回 false。
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        ViewBag.Success = true;
                        return PartialView("_ChangePasswordPartial");
                    }
                    else
                    {
                        ModelState.AddModelError("", "当前密码不正确或新密码无效。");
                    }
                }
            }
            else
            {
                // 用户没有本地密码，因此将删除由于缺少
                // OldPassword 字段而导致的所有验证错误
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return PartialView();
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // 如果我们进行到这一步时某个地方出错，则重新显示表单
            return PartialView("_ChangePasswordPartial",model);
        }

        ////
        //// POST: /Account/ExternalLogin

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult ExternalLogin(string provider, string returnUrl)
        //{
        //    return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        //}

        ////
        //// GET: /Account/ExternalLoginCallback

        //[AllowAnonymous]
        //public ActionResult ExternalLoginCallback(string returnUrl)
        //{
        //    AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        //    if (!result.IsSuccessful)
        //    {
        //        return RedirectToAction("ExternalLoginFailure");
        //    }

        //    if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
        //    {
        //        return RedirectToLocal(returnUrl);
        //    }

        //    if (User.Identity.IsAuthenticated)
        //    {
        //        // 如果当前用户已登录，则添加新帐户
        //        OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);
        //        return RedirectToLocal(returnUrl);
        //    }
        //    else
        //    {
        //        // 该用户是新用户，因此将要求该用户提供所需的成员名称
        //        string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
        //        ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
        //        ViewBag.ReturnUrl = returnUrl;
        //        return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
        //    }
        //}

        ////
        //// POST: /Account/ExternalLoginConfirmation

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
        //{
        //    string provider = null;
        //    string providerUserId = null;

        //    if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
        //    {
        //        return RedirectToAction("Manage");
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        // 将新用户插入到数据库
        //        using (UsersContext db = new UsersContext())
        //        {
        //            UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
        //            // 检查用户是否已存在
        //            if (user == null)
        //            {
        //                // 将名称插入到配置文件表
        //                db.UserProfiles.Add(new UserProfile { UserName = model.UserName });
        //                db.SaveChanges();

        //                OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
        //                OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);

        //                return RedirectToLocal(returnUrl);
        //            }
        //            else
        //            {
        //                ModelState.AddModelError("UserName", "用户名已存在。请输入其他用户名。");
        //            }
        //        }
        //    }

        //    ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
        //    ViewBag.ReturnUrl = returnUrl;
        //    return View(model);
        //}

        ////
        //// GET: /Account/ExternalLoginFailure

        //[AllowAnonymous]
        //public ActionResult ExternalLoginFailure()
        //{
        //    return View();
        //}

        //[AllowAnonymous]
        //[ChildActionOnly]
        //public ActionResult ExternalLoginsList(string returnUrl)
        //{
        //    ViewBag.ReturnUrl = returnUrl;
        //    return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        //}

        //[ChildActionOnly]
        //public ActionResult RemoveExternalLogins()
        //{
        //    ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
        //    List<ExternalLogin> externalLogins = new List<ExternalLogin>();
        //    foreach (OAuthAccount account in accounts)
        //    {
        //        AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

        //        externalLogins.Add(new ExternalLogin
        //        {
        //            Provider = account.Provider,
        //            ProviderDisplayName = clientData.DisplayName,
        //            ProviderUserId = account.ProviderUserId,
        //        });
        //    }

        //    ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
        //    return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        //}
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChangeAvatar(string avatardata, string filetype, string x, string y, string w, string h)
        {
            byte[] b = System.Convert.FromBase64String(avatardata);
            if (avatardata == null || !filetype.Contains("image") || b.Length > 524288 * 2)
            {
                ModelState.AddModelError("", "头像必须是图片，且小于1MB。");
            }
            else
            {
                int width, height, xcoord, ycoord;
                if(!(int.TryParse(x, out xcoord) && int.TryParse(y, out ycoord) && int.TryParse(w,out width) && int.TryParse(h, out height))){
                    xcoord = 0; ycoord = 0; width=150;height=150;
                }
                
                BlogUtil.UpdateOrAddPic(User.Identity.Name, filetype, BlogUtil.Crop(b,width, height, xcoord, ycoord));
                ViewBag.ChangeSuccess = "修改成功";
            }
            return PartialView("_ChangeAvatarPartial");
        }

        public ActionResult ChangeEmail(string email=null)
        {
            int userid = WebSecurity.CurrentUserId;
            UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserId == userid);
            ViewBag.Email = user.Email;
            if (!string.IsNullOrWhiteSpace(email))
            {
                user.Email = email;
                db.SaveChanges();
                ViewBag.Success = true;
                ViewBag.Email = email;
            }
            return PartialView("_ChangeEmailPartial");
        }
        [HttpGet]
        public ActionResult ChangeComment()
        {
            int userid = WebSecurity.CurrentUserId;
            UserProfile user = db.UserProfiles.Find(WebSecurity.CurrentUserId);
            ViewBag.Comment = user.UserComment;
            return PartialView();
        }
        [HttpPost]
        public ActionResult ChangeComment(string comment)
        {
            int userid = WebSecurity.CurrentUserId;
            UserProfile user = db.UserProfiles.Find(WebSecurity.CurrentUserId);
            ViewBag.Comment = user.UserComment;
            if (comment.Length <= 200 && user.UserComment != comment)
            {
                user.UserComment = comment;
                db.SaveChanges();
                ViewBag.Success = true;
                ViewBag.Comment = comment;
            }
            return PartialView();
        }
        [HttpGet]
        public PartialViewResult UserOptions()
        {
            UserOption model = null;
            UserProfile user = db.UserProfiles.Find(WebSecurity.CurrentUserId);
            model = user.option;
            return PartialView(model);
        }
        [HttpPost]
        public PartialViewResult UserOptions(UserOption model)
        {
            UserProfile user = db.UserProfiles.Find(WebSecurity.CurrentUserId);
            if (user.option == null)
            {
                user.option = model;
            }
            else
            {
                user.option.sendNoticeForNewReply = model.sendNoticeForNewReply;
                user.option.sendNoticeForNewPostReply = model.sendNoticeForNewPostReply;
            }
            db.SaveChanges();
            ViewBag.Success = true;
            return PartialView(model);
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPW(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return HttpNotFound();
            }
            return View(new PasswordResetModel { Code = token });
        }
        [HttpPost]
        [AllowAnonymous, ValidateAntiForgeryToken]
        public ViewResult ResetPW(PasswordResetModel model)
        {
            if (ModelState.IsValid)
            {
                if(WebSecurity.ResetPassword(model.Code, model.Password)){
                    ViewBag.ok = true;
                }
                else{
                    ModelState.AddModelError("Code", "验证码错误，请检查重试。如果验证码已经过期，请重新进行申请。");
                }
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ViewResult Forget()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous, ValidateAntiForgeryToken]
        public JsonResult Forget(string email, string Captcha)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(Captcha))
            {
                return Json(new { err = "请输入邮箱地址和验证码。" });
            }
            else if (BlogUtil.checkCaptchaError(Captcha, null))
            {
                return Json(new { err = "验证码计算错误，请重试。" });
            }
            else
            {
                DateTime? lastpost = Session["LastPostTime"] as DateTime?;
                if (lastpost.HasValue)
                {
                    var diff = DateTime.Now - lastpost.Value;
                    if (diff.TotalSeconds < 60)
                    {
                        return Json(new { err = "请不要短时间内多次发送邮件！"});
                    }
                }
                UserProfile user = db.UserProfiles.SingleOrDefault(u => u.Email == email);
                if (user == null)
                {
                    return Json(new { err = "该电子邮件地址未被注册。"});
                }
                else
                {
                    BlogUtil.sendPWEmailForUser(user, Url.Action("ResetPW",null, null, Request.Url.Scheme)+ "?token=");
                    Session["LastPostTime"] = DateTime.Now;
                }
            }
            return Json(new { ok = true});
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult CheckUsername(string username)
        {
            if (WebSecurity.UserExists(username))
            {
                return Json("该用户名已注册", JsonRequestBehavior.AllowGet);
            }
            else
                return Json(true);
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult CheckEmail(string Email)
        {
            UserProfile user = db.UserProfiles.FirstOrDefault(u=>u.Email == Email);
            if (user != null)
            {
                if (user.UserId == WebSecurity.CurrentUserId)
                {
                    return Json(false);
                }
                return Json("该邮箱已被用户 " + user.UserName + " 注册", JsonRequestBehavior.AllowGet);
            }
            else
                return Json(true);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        
        #region 帮助程序
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // 请参见 http://go.microsoft.com/fwlink/?LinkID=177550 以查看
            // 状态代码的完整列表。
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "用户名已存在。请输入其他用户名。";

                case MembershipCreateStatus.DuplicateEmail:
                    return "该电子邮件地址的用户名已存在。请输入其他电子邮件地址。";

                case MembershipCreateStatus.InvalidPassword:
                    return "提供的密码无效。请输入有效的密码值。";

                case MembershipCreateStatus.InvalidEmail:
                    return "提供的电子邮件地址无效。请检查该值并重试。";

                case MembershipCreateStatus.InvalidAnswer:
                    return "提供的密码取回答案无效。请检查该值并重试。";

                case MembershipCreateStatus.InvalidQuestion:
                    return "提供的密码取回问题无效。请检查该值并重试。";

                case MembershipCreateStatus.InvalidUserName:
                    return "提供的用户名无效。请检查该值并重试。";

                case MembershipCreateStatus.ProviderError:
                    return "身份验证提供程序返回了错误。请验证您的输入并重试。如果问题仍然存在，请与系统管理员联系。";

                case MembershipCreateStatus.UserRejected:
                    return "已取消用户创建请求。请验证您的输入并重试。如果问题仍然存在，请与系统管理员联系。";

                default:
                    return "发生未知错误。请验证您的输入并重试。如果问题仍然存在，请与系统管理员联系。";
            }
        }
        #endregion
    }
}
